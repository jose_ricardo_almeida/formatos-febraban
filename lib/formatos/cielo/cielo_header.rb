class CieloHeader  < FormatSection
  require 'date'

  def initialize master
    super(master, true, true, false)

    @section = Section.new({
        0 => Position.new(1, 2, false, "00", true), # Código do Registro
        1 => Position.new(2, 8, true),              # Data do Deposito
        2 => Position.new(3, 7, true),              # Numero do Resumo de operações
        3 => Position.new(4, 10, false),            # Reservado para o estabelecimento
        4 => Position.new(5, 3, false),             # Constantes zeros
        5 => Position.new(6, 10, true),             # Numero do estabelecimento na Cielo
        6 => Position.new(7, 3, false),             # Código da moeda
        7 => Position.new(8, 1, false,"P"),         # Indicador do processo
        8 => Position.new(9, 1, false, "V"),        # Indicador da venda
        9 => Position.new(10, 1, false),            # Indicador de estabelecimento especial (Brancos)
        10 => Position.new(11, 204, false),         # Constante (Brancos)
    })
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Gerais
  def process_section file
    self.set_data_deposito         file[2..9]
    self.set_numero_lote           file[10..16]
    self.set_reservado_1           file[17..26]
    self.set_reservado_2           file[27..29]
    self.set_numero_cielo          file[30..39]
    self.set_moeda                 file[40..42]
    self.set_indicador_processo    file[43..43]
    self.set_indicador_venda       file[44..44]
    self.set_indicador_especial    file[45..45]
    self.set_reservado_3           file[46..249]
  end

  def get_data_deposito
    self.get_section_value(1)
  end

  def get_numero_lote
    self.get_section_value(2)
  end

  def get_numero_cielo
    self.get_section_value(5)
  end

  def get_moeda
    self.get_section_value(6)
  end

  def set_data_deposito valor
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%Y")

      if real_data.length == 8
        self.set_section_value(1, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_numero_lote valor
    self.set_section_value(2, valor)
  end

  def set_reservado_1 valor = ""
    self.set_section_value(3, valor)
  end

  def set_reservado_2 valor = "000"
    self.set_section_value(4, valor)
  end

  def set_numero_cielo valor
    self.set_section_value(5, valor)
  end

  def set_moeda valor
    if ["986","840"].include? valor
      self.set_section_value(6, valor)
    else
      raise "Código de moeda não aceito"
    end
  end

  def set_indicador_processo valor = "P"
    self.set_section_value(7, valor)
  end

  def set_indicador_venda valor = "V"
    self.set_section_value(8, valor)
  end

  def set_indicador_especial valor = ""
    self.set_section_value(9, valor)
  end

  def set_reservado_3 valor = ""
    self.set_section_value(10, valor)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    (self.get_data_deposito.length > 0 and
     self.get_numero_cielo.length > 0  and
     self.get_numero_lote.length > 0   and
     self.get_moeda.length > 0)
  end
end

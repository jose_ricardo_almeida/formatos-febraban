class CieloTrailler < FormatSection
  require 'date'

  def initialize master
    super(master, true, true, false)

    @section = Section.new({
               0 => Position.new(1, 2, false, "99", true),                 # Código do Registro
               1 => Position.new(2, 7, true),                              # Qtde de Registros
               2 => Position.new(3, 15, true),                             # Valor total bruto
               3 => Position.new(4, 15, true),                             # Valor total aceito
               4 => Position.new(5, 15, true),                             # Valor total liquido
               5 => Position.new(6, 8, true),                              # Data prevista do credito (Zeros na remessa)
               6 => Position.new(7, 188, false),                           # Reservado (Brancos)
           })
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Gerais
  def process_section file
    self.set_total_registros       file[2..8]
    self.set_valor_total           file[9..24]
    self.set_valor_aceito          file[25..39]
    self.set_valor_liquido         file[40..54]
    self.set_data_prevista         file[55..62]
    self.set_reservado             file[63..249]
  end


  def get_total_registros
    self.get_section_value(1)
  end

  def get_valor_total
    self.get_section_value(2).to_i
  end


  def set_total_registros registros
    registros = registros.to_i

    if registros > 0
      self.set_section_value(1, registros)
    else
      raise "#{get_id}: A quantidade de registros deve ser positiva e maior que 0
              Valor: #{registros}"
    end
  end

  def set_valor_total valor
    valor = valor.to_i

    if valor > 0
      self.set_section_value(2, valor)
    else
      raise "#{get_id}: A quantidade de registros deve ser positiva e maior que 0
              Valor: #{valor}"
    end
  end

  def set_valor_aceito valor = "0"
    self.set_section_value(3, valor)
  end

  def set_valor_liquido valor = "0"
    self.set_section_value(4, valor)
  end

  def set_data_prevista valor = "0"
    self.set_section_value(5, valor)
  end

  def set_reservado valor = ""
    self.set_section_value(6, valor)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    self.get_total_registros.length > 0 and self.get_valor_total > 0
  end

end
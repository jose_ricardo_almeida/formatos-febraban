class CieloDetalhe < FormatSection
  require 'date'

  def initialize master
    super(master, true, true, false)

    @section = Section.new({
      0 => Position.new(1, 2, false, "01", true), # Tipo do Registro
      1 => Position.new(2, 7, true),              # Numero Sequencial
      2 => Position.new(3, 19, true),             # Numero do Cartão
      3 => Position.new(4, 6, false, "000000"),   # Código de Autorização
      4 => Position.new(5, 8, false),             # Data da Venda
      5 => Position.new(6, 1, true),              # Opção da Venda
      6 => Position.new(7, 15, true),             # Valor da venda
      7 => Position.new(8, 3, true),              # Quantidade de Parcelas
      8 => Position.new(9, 15, true),             # Preencher com zeros
      9 => Position.new(10, 15, true),            # Preencher com zeros
      10 => Position.new(11, 15, true),           # Preencher com zeros
      11 => Position.new(12, 15, true),           # Valor da Parcela
      12 => Position.new(13, 7, true),            # Numero do resumo da operação
      13 => Position.new(14, 3, true),            # Preencher com zeros
      14 => Position.new(15, 10, true),           # Numero do estabelecimento
      15 => Position.new(16, 30, false),          # Reservado
      16 => Position.new(17, 2, true),            # Status da venda
      17 => Position.new(18, 8, true),            # Data prevista da liquidação
      18 => Position.new(19, 4, false),           # Validade do cartao
      19 => Position.new(20, 7, true),            # Preencher com zeros
      20 => Position.new(21, 15, true),           # Preencher com zeros
      21 => Position.new(22, 3, false),           # Reservado(Brancos)
      22 => Position.new(23, 4, false),           # Código do erro(Brancos na remessa)
      23 => Position.new(24, 11, false),          # Referencia (Brancos)
      24 => Position.new(25, 19, false),          # Cartao Novo(Brancos)
      25 => Position.new(26, 4, false),           # Vencimento Novo (Brancos)
      26 => Position.new(27, 2, false),           # Reservado(Brancos)
    })
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Gerais
  def process_section file
    self.set_sequencial         file[2..8]
    self.set_numero_cartao      file[9..27]
    self.set_codigo_autorizacao file[28..33]
    self.set_data_venda         file[34..41]
    self.set_opcao_venda        file[42..42]
    self.set_valor_venda        file[43..57]
    self.numero_parcelas        file[58..60]
    self.set_reservado_1        file[61..75]
    self.set_reservado_2        file[76..90]
    self.set_reservado_3        file[91..105]
    self.set_valor_parcela      file[106..120]
    self.set_numero_lote        file[121..127]
    self.set_reservado_4        file[128..130]
    self.set_numero_cielo       file[131..140]
    self.set_reservado_5        file[141..170]
    self.set_status_venda       file[171..172]
    self.set_data_liquidacao    file[173..180]
    self.set_validade_cartao    file[181..184]
    self.set_reservado_6        file[185..191]
    self.set_reservado_7        file[192..206]
    self.set_reservado_8        file[207..209]
    self.set_codigo_erro        file[210..213]
    self.set_reservado_9        file[214..224]
    self.set_cartao_novo        file[225..243]
    self.set_vencimento_novo    file[244..247]
    self.set_reservado_10       file[248..249]
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_sequencial
    self.get_section_value(1)
  end

  def get_numero_cartao
    self.get_section_value(2)
  end

  def get_codigo_autorizacao
    self.get_section_value(3)
  end

  def get_data_venda
    self.get_section_value(4)
  end

  def get_valor
    self.get_section_value(6).to_i
  end

  def get_numero_lote
    self.get_section_value(12)
  end

  def get_numero_cielo
    self.get_section_value(14)
  end

  def get_reservado
    self.get_section_value(15)
  end

  def get_status_venda
    self.get_section_value(16)
  end

  def get_data_liquidacao
    self.get_section_value(17)
  end

  def get_validade_cartao
    self.get_section_value(18)
  end

  def get_codigo_erro
    self.get_section_value(22)
  end

  def success?
    self.get_status_venda() == '00'
  end

  def soft_bounce?
    codes = ["04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20", "21",
             "22", "23", "54", "77", "81", "88", "95", "99"]

    !self.success? && codes.include?(self.get_status_venda())
  end

  def hard_bounce?
    !self.success? && !self.soft_bounce?
  end

  def get_mensagem_retorno
    case self.get_status_venda
      when "00"
        "Transação OK (Transação Aceita)"
      when "01"
        "Erro no arquivo"
      when "02"
        "Código de autorização inválido"
      when "03"
        "Estabelecimento inválido"
      when "04"
        "Lote misturado"
      when "05"
        "Número de parcelas inválido"
      when "06"
        "Diferença de valor no RO"
      when  "07"
        "Número do RO inválido (registro BH)"
      when "08"
        "Valor de entrada inválido"
      when  "09"
        "Valor da taxa de embarque inválido"
      when "10"
        "Valor da parcela inválido"
      when "11"
        "Código de transação inválido"
      when "12"
        "Transação inválida"
      when "13"
        "Valor inválido"
      when "14"
        "Não aplicável"
      when  "15"
        "Valor do cancelamento inválido"
      when "16"
        "Transação original não localizada (para cancelamento)"
      when "17"
        "Nº de itens informados no RO não compatível com os CV’s"
      when "18"
        "Número de referência inválido"
      when "20"
        "Cancelamento para parcelado de transação já cancelada"
      when "21"
        "Valor do cancelamento maior que o valor da venda"
      when "22"
        "Valor do cancelamento maior que o permitido (alçada)"
      when "23"
        "Número do RO original inválido (registro I2)"
      when "42"
        "Cartão cancelado"
      when "54"
        "Não é permitido cancelamento parcial de um plano parcelado que está sendo contestado pelo portador."
      when "56"
        "Tipo de plano de cartão inválido"
      when "59"
        "Tipo cartão inválido"
      when "60"
        "Data inválida"
      when "71"
        "Transação rejeitada pelo banco emissor"
      when "72"
        "Transação rejeitada pelo banco emissor"
      when "73"
        "Cartão com problema - reter o cartão"
      when "74"
        "Autorização negada"
      when "75"
        "Erro"
      when "76"
        "Transação rejeitada pelo banco emissor"
      when "77"
        "Erro de sintaxe - refaça a transação"
      when "78"
        "Não foi encontrada autorização no emissor"
      when "79"
        "Cartão cancelado"
      when "80"
        "Cartão cancelado"
      when "81"
        "Fundos insuficientes"
      when "82"
        "Cartão vencido ou data do vencimento errada"
      when "87"
        "Cartão não permitido"
      when "88"
        "Excedeu o número de transações no período"
      when "89"
        "Mensagem difere da mensagem original"
      when "92"
        "Banco emissor sem comunicação"
      when "93"
        "Cancelamento com mais de 365 dias"
      when "94"
        "Duplicidade de linhas aéreas"
      when "95"
        "Sem saldo em aberto"
      when "99"
        "Outros motivos"
    end
  end

  def get_mensagem_erro
    case self.get_codigo_erro
      when "E001"
        "Validar estrutura do Arquivo"
      when "E002"
        "Reveja os dados do cartão"
      when "E005"
        "Problema no seu cadastro na Cielo"
      when "E010"
        "Problema no seu cadastro na Cielo"
      when "E023"
        "Problema no seu cadastro na Cielo"
      when "E024"
        "Tipo de cartão não permitido neste canal"
      when "E044"
        "Data da transação não confere"
      when "E045"
        "Código de autorização não encontrado"
      when "E048"
        "Data de vencimento cartão incorreta"
      when "E054"
        "Estrutura do lote incorreta"
      when "E055"
        "Quantidade de Parcelas invalida"
      when "E056"
        "Transação não permitida para seu cadastro"
      when "E057"
        "Cartão cancelado"
      when "E061"
        "Reveja os dados do cartão"
      when "E094"
        "Transação não permitida para cartão Internacional"
      when "E095"
        "Transação não permitida para cartão Débito"
      when "E097"
        "Parcelado com valor inferior a R$ 5,00"
      when "E101"
        "Transação rejeitada pelo banco Emissor"
      when "E102"
        "Transação rejeitada pelo banco Emissor"
      when "E103"
        "Problema no seu cadastro na Cielo"
      when "E104"
        "Cartão cancelado"
      when "E105"
        "Transação rejeitada pelo banco Emissor"
      when "E107"
        "ransação rejeitada pelo banco Emissor"
      when "E112"
        "Validar Transação e Cartão"
      when "E114"
        "Reveja os dados do cartão"
      when "E141"
        "Cartão cancelado"
      when "E143"
        "Cartão cancelado"
      when "E151"
        "Transação não autorizada pelo banco Emissor"
      when "E154"
        "Data de vencimento cartão incorreta"
      when "E157"
        "Cartão cancelado"
      when "E158"
        "Tipo de cartão não permitido neste canal"
      when "E162"
        "Cartão não permitido para esta transação"
      when "E180"
        "Data da transação não confere"
      when "E182"
        "Código de segurança do cartão inválido"
      when "E191"
        "Banco Emissor sem comunicação"
      when "E192"
        "Aplicável para Cias Aéreas"
      when "E205"
        "Reveja os dados do cartão"
      when "E207"
        "Validar Número de Lote"
      when "E209"
        "Aplicável para Cias Aéreas"
      when "E210"
        "Validar Parcelas"
      when "E211"
        "Aplicável para Cias Aéreas"
      when "E212"
        "Aplicável para Cias Aéreas"
      when "E213"
        "Valor de cancelamento não permitido"
      when "E214"
        "Transação não localizada"
      when "E215"
        "Transação já cancelada"
      when "E216"
        "Transação cancelada anteriormente"
      when "E217"
        "Valor de cancelamento maior venda"
      when "E218"
        "Cancelamento não permitido neste canal"
      when "E219"
        "Número do RO não localizado"
      when "E220"
        "Prazo excedido para cancelar transação"
      when "E900"
        "Tipo de Registro Inválido"
      when "E901"
        "Data do Depósito do Header Inválida"
      when "E902"
        "Número do Resumo de Operações (RO) do Header Inválido"
      when "E903"
        "Número do Estabelecimento do Header Inválido"
      when "E904"
        "Moeda do header Inválida"
      when "E905"
        "Indicador de teste do Header Inválido"
      when "E906"
        "Indicador de venda do Header Inválido"
      when "E911"
        "Número de Comprovante de Venda(CV) Inválido"
      when "E912"
        "Data de Venda Inválida"
      when "E913"
        "Opção de Venda Inválida"
      when "E914"
        "Número do Cartão Inválido"
      when "E915"
        "Valor de Venda Inválido"
      when "E916"
        "Quantidade de Parcelas Inválida"
      when "E917"
        "Valor Financiado Inválido"
      when "E918"
        "Valor de Entrada Inválido"
      when "E919"
        "Valor da Taxa Embarque Inválido"
      when "E920"
        "Valor da Parcela Inválido"
      when "E921"
        "Número do Resumo de Operações (RO) Inválido"
      when "E922"
        "Número do Estabelecimento Inválido"
      when "E923"
        "Validade do Cartão Inválido"
      when "E924"
        "Número do Resumo de Operações (RO) Original Inválido"
      when "E926"
        "Valor do Reembolso Inválido"
      when "E930"
        "Quantidade de Registros do Trailer não confere"
      when "E931"
        "Valor total bruto não confere"
      else
        ""
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters
  def set_sequencial(valor)
    self.set_section_value(1, valor)
  end

  def set_numero_cartao(valor)
    self.set_section_value(2, valor)
  end

  def set_codigo_autorizacao(valor = "000000")
    self.set_section_value(3, valor)
  end

  def set_data_venda(valor)
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%Y")

      if real_data.length == 8
        self.set_section_value(4, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
            Valor: #{valor}"
    end
  end

  def set_opcao_venda(valor = "0")
    self.set_section_value(5, valor)
  end

  def set_valor_venda(valor)
    self.set_section_value(6, valor)
  end

  def numero_parcelas(valor = "0")
    self.set_section_value(7, valor)
  end

  def set_reservado_1(valor = "0")
    self.set_section_value(8, valor)
  end

  def set_reservado_2(valor = "0")
    self.set_section_value(9, valor)
  end

  def set_reservado_3(valor = "0")
    self.set_section_value(10, valor)
  end

  def set_valor_parcela(valor = "0")
    self.set_section_value(11, valor)
  end

  def set_numero_lote(valor)
    self.set_section_value(12, valor)
  end

  def set_reservado_4(valor = "0")
    self.set_section_value(13, valor)
  end

  def set_numero_cielo(valor)
    self.set_section_value(14, valor)
  end

  def set_reservado_5(valor = "")
    self.set_section_value(15, valor)
  end

  def set_status_venda(valor = "0")
    self.set_section_value(16, valor)
  end

  def set_data_liquidacao(valor = "0")
    self.set_section_value(17, valor)
  end

  #format yymm
  def set_validade_cartao(valor)
    if valor.is_a? String
      validade = valor[-2..-1] + valor[0..1]
    else
      validade = valor.strftime("%y%m")
    end

    self.set_section_value(18, validade)
  end

  def set_reservado_6(valor = "0")
    self.set_section_value(19, valor)
  end

  def set_reservado_7(valor = "0")
    self.set_section_value(20, valor)
  end

  def set_reservado_8(valor = "")
    self.set_section_value(21, valor)
  end

  def set_codigo_erro(valor = "")
    self.set_section_value(22, valor)
  end

  def set_reservado_9(valor = "")
    self.set_section_value(23, valor)
  end

  def set_cartao_novo(valor)
    self.set_section_value(24, valor)
  end

  def set_vencimento_novo(valor)
    self.set_section_value(25, valor)
  end

  def set_reservado_10(valor = "")
    self.set_section_value(26, valor)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    (self.get_sequencial.length > 0     and
    self.get_data_venda.length > 0      and
    self.get_numero_lote.length > 0     and
    self.get_numero_cielo.length > 0    and
    self.get_numero_cartao.length > 0   and
    self.get_validade_cartao.length > 0 and
    self.get_valor > 0)
  end
end

class Cielo
  require 'date'

  SECTION_TYPES = {header: '00', client: "01", trailler: '99'}
  VERSAO = "01"

  # Carrega todas as dependências de Layout
  Dir[File.join(File.dirname(__FILE__), '../..',  'base', '**/*.rb')].sort.each do |file|
    require file
  end

  # Partes do Layout
  require_relative 'cielo_header' # Header
  require_relative 'cielo_detalhe' # Clientes
  require_relative 'cielo_trailler' # Trailler

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Construtor
  def initialize file = ""
    @sections = []

    if file.size > 0
      self.process_file file
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Geral
  protected
  def add_section section
    if section.is_valid?
      self.sections << section
    else
      raise "Seção #{section.get_id} está inválida:
                #{section.errors.inspect}
            #{section.to_s}"
    end
  end

  def add_section_from_business section
    self.add_section section
    self
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Criação das Partes
  public

  # Seção Header
  def create_header(params = {})

    if self.get_header.nil?
      section = self.get_new_section(:header)


      section.set_data_deposito         params[:data_deposito]
      section.set_numero_lote           params[:numero_lote]
      section.set_numero_cielo          params[:numero_cielo]
      section.set_moeda                 params[:moeda]
      section.set_reservado_1
      section.set_reservado_2
      section.set_reservado_3
      section.set_indicador_processo
      section.set_indicador_venda
      section.set_indicador_especial

      self.add_section_from_business section

    else
      raise "Header já declarado!"
    end
  end

  # Seção Client
  def add_clients(params = {})

    self.valida_existe_header

    section = self.get_new_section(:client)

    section.set_sequencial            params[:sequencial]
    section.set_numero_cartao         params[:numero_cartao]
    section.set_data_venda            params[:data_venda]
    section.set_valor_venda           params[:valor]
    section.set_numero_lote           params[:numero_lote]
    section.set_numero_cielo          params[:numero_cielo]
    section.set_validade_cartao       params[:validade_cartao]
    section.set_codigo_autorizacao
    section.set_opcao_venda
    section.numero_parcelas
    section.set_reservado_1
    section.set_reservado_2
    section.set_reservado_3
    section.set_valor_parcela
    section.set_reservado_4
    section.set_reservado_5           params[:reservado]
    section.set_status_venda
    section.set_data_liquidacao
    section.set_reservado_6
    section.set_reservado_7
    section.set_reservado_8
    section.set_codigo_erro
    section.set_reservado_9
    section.set_cartao_novo           params[:cartao_novo]
    section.set_vencimento_novo       params[:vencimento_novo]
    section.set_reservado_10

    self.add_section_from_business section
  end

  # Seção Trailler
  def create_trailler
    self.valida_existe_header

    if self.get_trailler.nil?
      if self.get_section(SECTION_TYPES[:client]).length > 0
        section = self.get_new_section(:trailler)
        section.set_total_registros self.sections.length + 1
        section.set_valor_total     self.calculate_valor_total
        section.set_valor_aceito
        section.set_valor_liquido
        section.set_data_prevista
        section.set_reservado

        self.sections << section
      else
        raise "Nenhum valor declarado!"
      end
    else
      raise "Trailler já declarado!"
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  protected
  def valida_existe_header
    raise "Header ainda não declarado" if self.get_header.nil?
  end

  def valida_existe_trailler
    raise "Trailler ainda não declarado" if self.get_trailler.nil?
  end

  #--------------------------------------------------------------------------------------
  #--------------------------------------------------------------------------------------
  # Carregamento de Arquivo
  protected
  def get_new_section section_type
    case section_type
      when :header
        CieloHeader.new(self)
      when :client
        CieloDetalhe.new(self)
      when :trailler
        CieloTrailler.new(self)
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  public
  def sections
    @sections.to_a
  end

  def has_section section
    self.get_section(section).length > 0
  end

  def get_section section
    self.sections.select {|c| c.is_section?(section) == true }
  end

  def is_valid?
    self.sections.select {|b| b.is_valid? == false }.length == 0
  end

  def to_s
    self.sections.map{|a| a.to_s.concat("\r\n")}.join("")
  end

  def get_header
    self.get_section(SECTION_TYPES[:header]).first()
  end

  def get_trailler
    self.get_section(SECTION_TYPES[:trailler]).first()
  end

  #--------------------------------------------------------------------------------------
  #--------------------------------------------------------------------------------------
  # Utilitários

  protected
  def calculate_valor_total
    self.get_section(SECTION_TYPES[:client]).inject(0) do |sum, section|
      sum += section.get_valor
    end
  end

  #--------------------------------------------------------------------------------------
  #--------------------------------------------------------------------------------------
  # Carregamento de Arquivo
  protected
  def process_file location
    file = File.new(location, "r")

    while (line = file.gets)
      process_string line
    end

    file.close
  end

  def process_string file
    section_type = file[0,2]

    section = get_new_section SECTION_TYPES.key(section_type)
    section.process_section(file)

    self.add_section section
  end

end

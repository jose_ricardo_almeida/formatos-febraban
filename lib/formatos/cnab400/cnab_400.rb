class Cnab400
  require 'date'

  SECTION_TYPES = {header: '0', client: "1", trailler: '9'}

  attr_accessor :versao, :operacao

  # Carrega todas as dependências de Layout
  Dir[File.join(File.dirname(__FILE__), '../..',  'base', '**/*.rb')].sort.each do |file|
    require file
  end

  require_relative '../cnab400/arquivo/V08/header'
  require_relative '../cnab400/arquivo/V08/detalhe'
  require_relative '../cnab400/arquivo/V08/trailler'

  require_relative '../cnab400/arquivo/VAldeias/header'
  require_relative '../cnab400/arquivo/VAldeias/detalhe'
  require_relative '../cnab400/arquivo/VAldeias/trailler'

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Construtor
  def initialize(params = {})
    @sections = []
    @versao = params[:versao] || "V08"
    @operacao = params[:operacao]

    if params[:file] && params[:file].size > 0
      self.process_file params[:file]
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Geral
  protected
  def add_section section

    if section.is_valid?
      self.sections << section
    else
      raise "Seção #{section.get_id} está inválida:
                #{section.errors.inspect}
            #{section.to_s}"
    end

  end

  def add_section_from_business section
    self.add_section section
    self
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Criacao das Partes

  public

  def create_header(params = {})

    if self.get_header.nil?
      section = self.get_new_section(:header)

      section.set_values params

      self.add_section_from_business section

    else
      raise "Header já declarado!"
    end
  end

  # Seção Client
  def add_clients(params = {})

    self.valida_existe_header

    section = self.get_new_section(:client)

    section.set_values params
    self.add_section_from_business section
  end

  # Seção Trailler
  def create_trailler(params = {})
    self.valida_existe_header

    if self.get_trailler.nil?
      if self.get_section(SECTION_TYPES[:client]).length > 0
        section = self.get_new_section(:trailler)

        section.set_values params

        section.set_sequencial          self.sections.length + 1

        self.sections << section
      else
        raise "Nenhum valor declarado!"
      end
    else
      raise "Trailler já declarado!"
    end
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações

  def valida_existe_header
    raise "Header ainda não declarado" if self.get_header.nil?
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  public
  def sections
    @sections.to_a
  end

  def has_section section
    self.get_section(section).length > 0
  end

  def get_section section
    self.sections.select {|c| c.is_section?(section) == true }
  end

  def is_valid?
    self.sections.select {|b| b.is_valid? == false }.length == 0
  end

  def to_s
    self.sections.map{|a| a.to_s}.join("\r\n")
  end

  def get_header
    self.get_section(SECTION_TYPES[:header]).first()
  end

  def get_trailler
    self.get_section(SECTION_TYPES[:trailler]).first()
  end

  def get_new_section section_type
    case section_type
      when :header
        eval("Cnab400::#{@versao}::#{@operacao}::Header").new
      when :client
        eval("Cnab400::#{@versao}::#{@operacao}::Detalhe").new
      when :trailler
        eval("Cnab400::#{@versao}::#{@operacao}::Trailler").new
    end
  end

  #--------------------------------------------------------------------------------------
  #--------------------------------------------------------------------------------------
  # Carregamento de Arquivo
  protected
  def process_file location
    file = File.new(location, "r")

    while (line = file.gets)
      process_string line
    end

    file.close
  end

  def process_string file
    section_type = file[0]

    section = get_new_section SECTION_TYPES.key(section_type)
    section.process_section(file)

    self.add_section section
  end
end


module VAldeiasTraillerCommons

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    self.get_codigo_remessa > 0 and self.get_sequencial.length > 0
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_codigo_remessa
    self.get_section_value(1).to_i
  end

  def get_sequencial
    self.get_section_value(3)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters

  def set_total_registros valor = ""
    self.set_section_value(2, valor)
  end

  def set_mensagem valor = ""
    self.set_section_value(3, valor)
  end

  def set_sequencial valor = ""
    self.set_section_value(4, valor)
  end
end
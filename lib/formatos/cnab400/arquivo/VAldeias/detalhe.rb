module VAldeias
  require_relative 'detalhe_commons'

  module Remessa
    class Detalhe < FormatSection
      include VAldeiasDetalheCommons

      def initialize
        @section = Section.new({
          0 => Position.new(1, 1, false, "1", true),   # Código do Registro
          1 => Position.new(2, 5, true),                # Agencia de Débito
          2 => Position.new(3, 1, false, "3", true),   # Numero fixo aldeias
          3 => Position.new(4, 3, true, "0"),           # Zeros
          4 => Position.new(5, 7, true),                # Conta Corrente
          5 => Position.new(6, 1, false),               # Digito da CC
          6 => Position.new(7, 2, false),               # Carteira
          7 => Position.new(8, 3, true, "0"),           # Zeros
          8 => Position.new(9, 12, false),              # Nosso numero
          9 => Position.new(10, 4, true, "0"),          # Zeros
          10 => Position.new(11, 11, true),             # Numero Documento
          11 => Position.new(12, 4, true, "0"),         # Zeros
          12 => Position.new(13, 6, true),              # Data do documento
          13 => Position.new(14, 4, true, "0"),         # Zeros
          14 => Position.new(15, 6, true),              # Data de vencimento
          15 => Position.new(16, 40, false),            # Nome do sacado
          16 => Position.new(17, 60, false),            # Endereço
          17 => Position.new(18, 10, false),            # Numero
          18 => Position.new(19, 10, false),            # Complemento
          19 => Position.new(20, 30, false),            # Bairro
          20 => Position.new(21, 30, false),            # Cidade
          21 => Position.new(22, 2, false),             # Estado
          22 => Position.new(23, 8, false),             # Cep
          23 => Position.new(24, 15, true),             # Valor do titulo
          24 => Position.new(25, 44, false),            # Código de barras
          25 => Position.new(26, 4, true, "0"),        # Zeros
          26 => Position.new(27, 56, false),            # Linha digitavel
          27 => Position.new(28, 14, false),           # Brancos
          28 => Position.new(29, 6, true)              # Numero sequencial
        })
      end

      def set_values params
        set_agencia                 params[:agencia]
        set_conta_corrente          params[:conta_corrente]
        set_dv_conta_corrente       params[:dv_conta_corrente]
        set_carteira                params[:carteira]
        set_nosso_numero            params[:nosso_numero]
        set_numero_documento        params[:numero_documento]
        set_data_emissao_titulo     Date.today
        set_vencimento_titulo       params[:vencimento]
        set_nome_sacado             params[:nome_sacado]
        set_endereco                params[:endereco]
        set_numero                  params[:numero]
        set_complemento             params[:complemento]
        set_bairro                  params[:bairro]
        set_cidade                  params[:cidade]
        set_estado                  params[:estado]
        set_cep                     params[:cep]
        set_valor_titulo            params[:valor]
        set_codigo_barras           params[:codigo_barras]
        set_linha_digitavel         params[:linha_digitavel]
        set_branco
        set_sequencial              params[:sequencial]
      end
    end
  end

  module Retorno
    class Detalhe < FormatSection
      include VAldeiasDetalheCommons

      def initialize
        @section = Section.new({
           0 => Position.new(1, 1, false, "1", true),                 # Código do Registro
           1 => Position.new(2, 2, true),                              # Tipo de inscrição da empresa (01:CPF, 02: CNPJ)
           2 => Position.new(3, 14, true),                             # Numero da inscricao da empresa
           3 => Position.new(4, 3, false),                             # Reservado - Zeros
           4 => Position.new(5, 17, false),                            # Identificação da empresa (zero, carteira, agencia e CC )
           5 => Position.new(6, 25, false),                            # Numero controle do participante
           6 => Position.new(7, 8, true),                              # Reservado - Zeros
           7 => Position.new(8, 12, false),                            # Identificação do titulo no banco
           8 => Position.new(9, 10, false),                            # Reservado - Zeros
           9 => Position.new(10, 12, false),                           # Reservado - Zeros
           10 => Position.new(11, 1, false),                           # Indicador Rateio
           11 => Position.new(12, 2, false),                           # Reservado - Zeros
           12 => Position.new(13, 1, true),                            # Carteira
           13 => Position.new(14, 2, true),                            # Id da Ocorrencia
           14 => Position.new(15, 6, false),                           # Data da Ocorrencia
           15 => Position.new(16, 10, false),                          # Numero Documento
           16 => Position.new(17, 20, false),                          # Id do titulo no banco
           17 => Position.new(18, 6, false),                           # Data vencimento do titulo (DDMMAA)
           18 => Position.new(19, 13, true),                           # Valor do titulo
           19 => Position.new(20, 3, false),                           # Banco cobrador
           20 => Position.new(21, 5, false),                           # Agencia Cobradora
           21 => Position.new(22, 2, false),                           # Especie de titulo - branco
           22 => Position.new(23, 13, false),                          # Valor despesa das ocorrencias
           23 => Position.new(24, 13, false),                          # Outras despesas
           24 => Position.new(25, 13, false),                          # Juros operação em atraso
           25 => Position.new(26, 13, false),                          # IOF devido
           26 => Position.new(27, 13, false),                          # Valor abatimento
           27 => Position.new(28, 13, false),                          # Valor desconto concedido
           28 => Position.new(29, 13, false),                          # Valor pago
           29 => Position.new(30, 13, false),                          # Juros de mora
           30 => Position.new(31, 13, false),                          # Outros creditos
           31 => Position.new(32, 2, false),                           # Reservado - branco
           32 => Position.new(33, 1, false),                           # Motivo do codigo de ocorrencia
           33 => Position.new(34, 6, false),                           # Data do credito
           34 => Position.new(35, 3, false),                           # Origem pagamento
           35 => Position.new(36, 10, false),                          # Reservado - brancos
           36 => Position.new(37, 4, false),                           # Codigo do banco - quando cheque
           37 => Position.new(38, 10, false),                          # Motivo das rejeicoes
           38 => Position.new(39, 40, false),                          # Reservado - brancos
           39 => Position.new(40, 2, false),                           # numero do cartorio
           40 => Position.new(41, 10, false),                          # numero do protocolo
           41 => Position.new(42, 14, false),                          # Reservado - brancos
           42 => Position.new(43, 6, true)                             # Numero sequencial
         })
      end

      def processa_section file
        self.set_tipo_inscricao             file[1..2]
        self.set_numero_inscricao           file[3..16]
        self.set_zeros_1                    file[17..19]
        self.set_id_empresa                 file[20..36]
        self.set_controle_participante      file[37..61]
        self.set_zeros_2                    file[62..69]
        self.set_id_titulo                  file[70..81]
        self.set_zeros_3                    file[82..91]
        self.set_zeros_4                    file[93..103]
        self.set_indicador_rateio_retorno   file[104..104]
        self.set_zeros_5                    file[105..106]
        self.set_carteira_retorno           file[107..107]
        self.set_ocorrencia_retorno         file[108..109]
        self.set_data_ocorrencia            file[110..115]
        self.set_numero_documento_retorno   file[116..125]
        self.set_id_titulo                  file[126..145]
        self.set_data_titulo                file[146..151]
        self.set_valor_titulo_retorno       file[152..164]
        self.set_banco_cobrador_retorno     file[165..167]
        self.set_agencia_cobradora          file[168..172]
        self.set_especie_titulo_retorno     file[173..174]
        self.set_despesas                   file[175..187]
        self.set_outras_despesas            file[188..200]
        self.set_juros                      file[201..213]
        self.set_iof                        file[214..226]
        self.set_abatimento_concedido       file[227..239]
        self.set_desconto_concedido         file[240..252]
        self.set_valor_pago                 file[253..265]
        self.set_juros_mora                 file[266..278]
        self.set_outros_creditos            file[279..291]
        self.set_brancos_1                  file[292..293]
        self.set_motivo_ocorrencia          file[294..294]
        self.set_data_credito               file[295..300]
        self.set_origem_pagamento           file[301..303]
        self.set_brancos_2                  file[304..313]
        self.set_codigo_banco_retorno       file[314..317]
        self.set_motivo_rejeicoes           file[318..327]
        self.set_brancos_3                  file[328..367]
        self.set_numero_cartorio            file[368..369]
        self.set_numero_protocolo           file[370..379]
        self.set_brancos_4                  file[380..393]
        self.set_sequencial_retorno         file[394..399]
      end

    end
  end
end


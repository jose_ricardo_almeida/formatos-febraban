module VAldeiasDetalheCommons

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    self.get_nosso_numero.length > 0            and
    self.get_numero_documento.length > 0        and
    self.get_sequencial.length > 0
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_agencia
    self.get_section_value(1)
  end

  def get_nosso_numero
    self.get_section_value(8)
  end

  def get_numero_documento
    self.get_section_value(10)
  end

  def get_vencimento_titulo
    self.get_section_value(14)
  end

  def get_nome_sacado
    self.get_section_value(15)
  end

  def get_endereco
    self.get_section_value(16)
  end

  def get_sequencial
    self.get_section_value(28)
  end


  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters
  def set_agencia valor = ""
    self.set_section_value(1, valor)
  end

  def set_zeros_1 valor = "0"
    self.set_section_value(3, valor)
  end

  def set_numero_inscricao valor = ""
    self.set_section_value(2, valor)
  end

  def set_conta_corrente valor = ""
    self.set_section_value(4, valor)
  end

  def set_dv_conta_corrente valor = ""
    self.set_section_value(5, valor)
  end

  def set_carteira valor = ""
    self.set_section_value(6, valor)
  end

  def set_zeros_2 valor = "0"
    self.set_section_value(7, valor)
  end

  def set_nosso_numero valor
    self.set_section_value(8, valor)
  end

  def set_zeros_3 valor = "0"
    self.set_section_value(9, valor)
  end

  def set_numero_documento valor = ""
    self.set_section_value(10, valor)
  end

  def set_zeros_4 valor = "0"
    self.set_section_value(11, valor)
  end

  def set_data_emissao_titulo valor = ""
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(12, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_zeros_5 valor = "0"
    self.set_section_value(13, valor)
  end

  def set_vencimento_titulo valor
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(14, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_nome_sacado valor = ""
    self.set_section_value(15, valor)
  end

  def set_endereco valor = ""
    self.set_section_value(16, valor)
  end

  def set_numero valor = ""
    self.set_section_value(17, valor)
  end

  def set_complemento valor = ""
    self.set_section_value(18, valor)
  end

  def set_bairro valor = ""
    self.set_section_value(19, valor)
  end

  def set_cidade valor = ""
    self.set_section_value(20, valor)
  end

  def set_estado valor = ""
    self.set_section_value(21, valor)
  end

  def set_cep valor = ""
    self.set_section_value(22, valor)
  end

  def set_valor_titulo valor = ""
    self.set_section_value(23, valor)
  end

  def set_codigo_barras valor = ""
    self.set_section_value(24, valor)
  end

  def set_zeros_6 valor = "0"
    self.set_section_value(25, valor)
  end

  def set_linha_digitavel valor = ""
    self.set_section_value(26, valor)
  end

  def set_branco valor = " "
    self.set_section_value(27, valor)
  end

  def set_sequencial valor = ""
    self.set_section_value(28, valor)
  end
end
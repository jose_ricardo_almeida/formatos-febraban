module VAldeias
  require_relative 'header_commons'

  module Remessa
    class Header < FormatSection
      include VAldeiasHeaderCommons

      def initialize
        @section = Section.new({
          0 => Position.new(1, 1, false, "0", true),   # Código do Registro
          1 => Position.new(2, 1, false, "1"),         # Código Arquivo (1-Remessa, 2-Retorno)
          2 => Position.new(3, 7, false, "REMESSA"),   # Identificação literal do arquivo (REMESSA/RETORNO)
          3 => Position.new(4, 2, false, "01"),        # Código do serviço
          4 => Position.new(5, 15, false, "COBRANCA"), # Literal do serviço (COBRANÇA)
          5 => Position.new(6, 20, true),              # Código da Empresa
          6 => Position.new(7, 30, false),             # Nome da Empresa
          7 => Position.new(8, 3, true),               # Número do Banco (BRADESCO: 237)
          8 => Position.new(9, 15, false),             # Nome do Banco
          9 => Position.new(10, 6, false),             # Data de gravacao do arquivo (DDMMAA)
          10 => Position.new(11, 8, false),            # Reservado pelo Sistema - branco
          11 => Position.new(12, 9, false),            # Identificacao do sistema
          12 => Position.new(13, 276, false),          # Reservado pelo Sistema - branco
          13 => Position.new(14, 6, false, "000001")   # Numero sequencial no arquivo
        })
      end

      def set_values params
        set_codigo_empresa        params[:codigo]
        set_nome_empresa          params[:nome_empresa]
        set_numero_banco          params[:numero_banco]
        set_nome_banco            params[:nome_banco]
        set_data_gravacao         Date.today
        set_reservado_1
        set_identificacao_sistema params[:identificacao_empresa]
        set_sequencial            params[:sequencial]
      end
    end
  end

  module Retorno
    class Header < FormatSection
      include VAldeiasHeaderCommons

      def initialize
        @section = Section.new({
          0 => Position.new(1, 1, false, "0", true),   # Código do Registro
          1 => Position.new(2, 1, false, "2"),         # Código Arquivo (1-Remessa, 2-Retorno)
          2 => Position.new(3, 7, false, "RETORNO"),   # Identificação literal do arquivo (REMESSA/RETORNO)
          3 => Position.new(4, 2, false, "01"),        # Código do serviço
          4 => Position.new(5, 15, false, "COBRANCA"), # Literal do serviço (COBRANÇA)
          5 => Position.new(6, 20, true),              # Código da Empresa
          6 => Position.new(7, 30, false),             # Nome da Empresa
          7 => Position.new(8, 3, true),               # Número do Banco (BRADESCO: 237)
          8 => Position.new(9, 15, false),             # Nome do Banco
          9 => Position.new(10, 6, false),             # Data de gravacao do arquivo (DDMMAA)
          10 => Position.new(11, 8, false),            # Densidade da gravacao do arquivo
          11 => Position.new(12, 5, false),            # Numero do aviso bancario
          12 => Position.new(13, 266, false),          # Reservado pelo Sistema - branco
          13 => Position.new(14, 6, false),            # Data do crédito (DDMMAA)
          14 => Position.new(15, 9, false),            # Reservado pelo Sistema - branco
          15 => Position.new(16, 6, false)             # Numero sequencial no arquivo
        })
      end

      def process_section file
        self.set_codigo_arquivo     file[1..1]
        self.set_literal_arquivo    file[2..8]
        self.set_codigo_servico     file[9..10]
        self.set_literal_servico    file[11..25]
        self.set_codigo_empresa     file[26..45]
        self.set_nome_empresa       file[46..75]
        self.set_numero_banco       file[76..78]
        self.set_nome_banco         file[79..93]
        self.set_data_gravacao      file[94..99]
        self.set_densidade_arquivo  file[100..107]
        self.set_numero_aviso       file[108..112]
        self.set_brancos_1          file[113..378]
        self.set_data_credito       file[379..384]
        self.set_brancos_2          file[385..393]
        self.set_sequencial_retorno file[394..399]
      end
    end
  end
end

module V08TraillerCommons

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_codigo_remessa
    self.get_section_value(1).to_i
  end

  def get_sequencial
    self.get_section_value(3)
  end

  def get_codigo_arquivo
    self.get_section_value(1)
  end

  def get_total_titulos
    self.get_section_value(5)
  end

  def get_total_cobranca
    self.get_section_value(6)
  end

  def get_sequencial_retorno
    self.get_section_value(28)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters

  def set_branco valor = " "
    self.set_section_value(1, valor)
  end

  def set_codigo_arquivo valor = ""
    self.set_section_value(1, valor)
  end

  def set_sequencial valor
    self.set_section_value(2, valor.to_s)
  end

  def set_tipo_registro valor = ""
    self.set_section_value(2, valor)
  end

  def set_codigo_banco valor = ""
    self.set_section_value(3, valor)
  end

  def set_reservado_1 valor = " "
    self.set_section_value(4, valor)
  end

  def set_qtde_titulos valor = ""
    self.set_section_value(5, valor)
  end

  def set_valor_cobranca valor = ""
    self.set_section_value(6, valor)
  end

  def set_numero_aviso valor = ""
    self.set_section_value(7, valor)
  end

  def set_reservado_2 valor = " "
    self.set_section_value(8, valor)
  end

  def set_qtde_confirmados valor = ""
    self.set_section_value(9, valor)
  end

  def set_valor_confirmados valor = ""
    self.set_section_value(10, valor)
  end

  def set_valor_liquidados valor = ""
    self.set_section_value(11, valor)
  end

  def set_qtde_liquidados valor = ""
    self.set_section_value(12, valor)
  end

  def set_valor_ocorrencia_06 valor = ""
    self.set_section_value(13, valor)
  end

  def set_qtde_baixados valor = ""
    self.set_section_value(14, valor)
  end

  def set_valor_baixados valor = ""
    self.set_section_value(15, valor)
  end

  def set_qtde_abatimento_cancelado valor = ""
    self.set_section_value(16, valor)
  end

  def set_valor_abatimento_cancelado valor = ""
    self.set_section_value(17, valor)
  end

  def set_qtde_vencimento_alterado valor = ""
    self.set_section_value(18, valor)
  end

  def set_valor_vencimento_alterado valor = ""
    self.set_section_value(19, valor)
  end

  def set_qtde_abatimento_concedido valor = ""
    self.set_section_value(20, valor)
  end

  def set_valor_abatimento_concedido valor = ""
    self.set_section_value(21, valor)
  end

  def set_qtde_protesto_confirmado valor = ""
    self.set_section_value(22, valor)
  end

  def set_valor_protesto_confirmado valor = ""
    self.set_section_value(23, valor)
  end

  def set_reservado_3 valor = " "
    self.set_section_value(24, valor)
  end

  def set_valor_rateios valor = ""
    self.set_section_value(25, valor)
  end

  def set_qtde_rateios valor = ""
    self.set_section_value(26, valor)
  end

  def set_reservado_4 valor = ""
    self.set_section_value(27, valor)
  end

  def set_sequencial_retorno valor = ""
    self.set_section_value(28, valor)
  end
end

module V08HeaderCommons

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Validações
  def is_valid?
    self.get_codigo_arquivo > 0        and
    self.get_data_gravacao.length > 0         and
    self.get_nome_empresa.length > 0          and
    self.get_identificacao_sistema.length > 0 and
    self.get_nome_banco.length > 0
  end


  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_codigo_arquivo
    self.get_section_value(1).to_i
  end

  def get_identificacao_arquivo
    self.get_section_value(2)
  end

  def get_codigo_servico
    self.get_section_value(3)
  end

  def get_literal_servico
    self.get_section_value(4)
  end

  def get_codigo_empresa
    self.get_section_value(5)
  end

  def get_nome_empresa
    self.get_section_value(6)
  end

  def get_numero_banco
    self.get_section_value(7)
  end

  def get_nome_banco
    self.get_section_value(8)
  end

  def get_data_gravacao
    self.get_section_value(9)
  end

  def get_identificacao_sistema
    self.get_section_value(11)
  end

  def get_sequencial_remessa
    self.get_section_value(12)
  end

  def get_data_credito
    self.get_section_value(13)
  end

  def get_sequencial
    self.get_section_value(14)
  end

  def get_sequencial_retorno
    self.get_section_value(15)
  end

  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters
  def set_codigo_arquivo valor = ""
    self.set_section_value(1, valor)
  end

  def set_identificacao_arquivo valor = ""
    self.set_section_value(2, valor)
  end

  def set_literal_arquivo valor = ""
    self.set_section_value(2, valor)
  end

  def set_codigo_servico valor = ""
    self.set_section_value(3, valor)
  end

  def set_literal_servico valor = ""
    self.set_section_value(4, valor)
  end

  def set_codigo_empresa valor = ""
    self.set_section_value(5, valor)
  end

  def set_nome_empresa valor = ""
    self.set_section_value(6, valor)
  end

  def set_numero_banco valor = ""
    self.set_section_value(7, valor)
  end

  def set_nome_banco valor = ""
    self.set_section_value(8, valor)
  end

  def set_data_gravacao valor = ""
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(9, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_reservado_1 valor = " "
    self.set_section_value(10, valor)
  end

  def set_densidade_arquivo valor = " "
    self.set_section_value(10, valor)
  end

  def set_identificacao_sistema valor
    self.set_section_value(11, valor)
  end

  def set_numero_aviso valor = ""
    self.set_section_value(11, valor)
  end

  def set_sequencial_remessa valor = ""
    self.set_section_value(12, valor)
  end

  def set_brancos_1 valor = " "
    self.set_section_value(12, valor)
  end

  def set_reservado_2 valor = " "
    self.set_section_value(13, valor)
  end

  def set_data_credito valor = ""
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(13, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_sequencial valor = ""
    self.set_section_value(14, valor)
  end

  def set_brancos_2 valor = " "
    self.set_section_value(14, valor)
  end

  def set_sequencial_retorno valor = ""
    self.set_section_value(15, valor)
  end
end

module V08
  require_relative 'trailler_commons'

  module Remessa
    class Trailler < FormatSection
      include V08TraillerCommons

      def initialize
        @section = Section.new({
           0 => Position.new(1, 1, false, "9", true),                  # Código do Registro
           1 => Position.new(2, 393, false, " "),                      # Reservado - Brancos
           2 => Position.new(3, 6, true),                              # Numero sequencial do registro
         })
      end

      def set_value params
      end

      def set_values params
      end

      def is_valid?
        get_codigo_remessa > 0 and
        get_sequencial.length > 0
      end
    end
  end

  module Retorno
    class Trailler < FormatSection
      include V08TraillerCommons

      def initialize
        @section = Section.new({
           0 => Position.new(1, 1, false, "9", true),                 # Código do Registro
           1 => Position.new(2, 1, true, "2"),                         # Código Arquivo (1-Remessa, 2-Retorno)
           2 => Position.new(3, 2, true, "01"),                        # Id tipo de registro
           3 => Position.new(4, 3, true),                              # Codigo do banco
           4 => Position.new(5, 10, false),                            # Reservado - branco
           5 => Position.new(6, 8, true),                              # Qtde de titulo na cobranca
           6 => Position.new(7, 14, true),                             # Valor total em cobranca
           7 => Position.new(8, 8, false),                             # Numero aviso bancario
           8 => Position.new(9, 10, false),                            # Reservado - branco
           9 => Position.new(10, 5, false),                            # Qtde de registros - confirmacao
           10 => Position.new(11, 12, false),                          # Valor dos registros - confirmacao
           11 => Position.new(12, 12, false),                          # Valor dos registros - liquidacao
           12 => Position.new(13, 5, false),                           # Qtde dos registros - liquidacao
           13 => Position.new(14, 12, false),                          # Valor dos registros - ocorrencia 06
           14 => Position.new(15, 5, false),                           # Qtde dos registros - baixados
           15 => Position.new(16, 12, false),                          # Valor dos registros - baixados
           16 => Position.new(17, 5, false),                           # Qtde dos registros - abatimento cancelado
           17 => Position.new(18, 12, false),                          # Valor dos registros - abatimento cancelado
           18 => Position.new(19, 5, false),                           # Qtde dos registros - vencimento alterado
           19 => Position.new(20, 12, false),                          # Valor dos registros - vencimento alterado
           20 => Position.new(21, 5, false),                           # Qtde dos registros - abatimento concedido
           21 => Position.new(22, 12, false),                          # Valor dos registros - abatimento concedido
           22 => Position.new(23, 5, false),                           # Qtde dos registros - confirmacao instrucao protesto
           23 => Position.new(24, 12, false),                          # Valor dos registros - confirmacao instrucao protesto
           24 => Position.new(25, 174, false),                         # Reservado - branco
           25 => Position.new(26, 15, false),                          # Valor total rateios
           26 => Position.new(27, 8, false),                           # Qtde total rateios
           27 => Position.new(28, 9, false),                           # Reservado - branco
           28 => Position.new(29, 6, true)                             # Numero sequencial do registro
         })
      end

      def process_section file
        set_codigo_arquivo             file[1..1]
        set_tipo_registro              file[2..3]
        set_codigo_banco               file[4..6]
        set_reservado_1                file[7..16]
        set_qtde_titulos               file[17..24]
        set_valor_cobranca             file[25..38]
        set_numero_aviso               file[39..46]
        set_reservado_2                file[47..56]
        set_qtde_confirmados           file[57..61]
        set_valor_confirmados          file[62..73]
        set_valor_liquidados           file[74..85]
        set_qtde_liquidados            file[86..90]
        set_valor_ocorrencia_06        file[91..102]
        set_qtde_baixados              file[103..107]
        set_valor_baixados             file[108..119]
        set_qtde_abatimento_cancelado  file[120..124]
        set_valor_abatimento_cancelado file[125..136]
        set_qtde_vencimento_alterado   file[137..141]
        set_valor_vencimento_alterado  file[142..153]
        set_qtde_abatimento_concedido  file[154..158]
        set_valor_abatimento_concedido file[159..170]
        set_qtde_protesto_confirmado   file[171..175]
        set_valor_protesto_confirmado  file[176..187]
        set_reservado_3                file[188..361]
        set_valor_rateios              file[362..376]
        set_qtde_rateios               file[377..384]
        set_reservado_4                file[385..393]
        set_sequencial_retorno         file[394..399]
      end

      def is_valid?
        get_codigo_arquivo.length > 0 and
        get_total_titulos.length > 0  and
        get_total_cobranca.length     and
        get_sequencial_retorno.length
      end

    end
  end

end

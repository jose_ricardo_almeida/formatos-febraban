module V08
  require_relative 'detalhe_commons'

  module Remessa
    class Detalhe < FormatSection
      include V08DetalheCommons

      def initialize
        @section = Section.new({
          0 => Position.new(1, 1, false, "1", true),                  # Código do Registro
          1 => Position.new(2, 5, false),                             # Agencia de Débito (opcional)
          2 => Position.new(3, 1, false),                             # Digito Agencia (opcional)
          3 => Position.new(4, 5, false),                             # Razao da Conta Corrente (opcional)
          4 => Position.new(5, 7, false),                             # Conta Corrente (opcional)
          5 => Position.new(6, 1, false),                             # Digito da CC (opcional)
          6 => Position.new(7, 17, false),                            # Identificação da empresa (zero, carteira, agencia e CC )
          7 => Position.new(8, 25, false),                            # Numero controle do participante
          8 => Position.new(9, 3, false),                             # Numero do Banco ("237")
          9 => Position.new(10, 1, true),                             # Campo de multa (0: Sem Multa/ 2: Com multa)
          10 => Position.new(11, 4, true),                            # Percentual de multa
          11 => Position.new(12, 11, true),                           # Identificação do titulo no banco
          12 => Position.new(13, 1, true),                            # Digito de AutoConferencia
          13 => Position.new(14, 10, true),                           # Desconto Bonificacao por dia
          14 => Position.new(15, 1, false, "2"),                      # Condicao para emissao da cobranca
          15 => Position.new(16, 1, false),                           # Id se emite Boleto para Debito Automatico (Diferente de N registra e emite boleto)
          16 => Position.new(17, 10, false),                          # Reservado - Brancos
          17 => Position.new(18, 1, false),                           # Indicador Rateio (opcional)
          18 => Position.new(19, 1, false),                           # Enderecamento para aviso de Debito em Cc (opcional)
          19 => Position.new(20, 2, false),                           # Reservado - Brancos
          20 => Position.new(21, 2, false, "01"),                     # Identificacao de ocorrencias
          21 => Position.new(22, 10, false),                          # Numero do documento
          22 => Position.new(23, 6, false),                           # Data vencimento do titulo (DDMMAA)
          23 => Position.new(24, 13, true),                           # Valor do titulo
          24 => Position.new(25, 3, true, "0"),                       # Banco cobrador - Zeros
          25 => Position.new(26, 5, true, "0"),                       # Agencia depositaria - Zeros
          26 => Position.new(27, 2, false, "99"),                     # Tipo do titulo
          27 => Position.new(28, 1, false, "N"),                      # Identificação
          28 => Position.new(29, 6, false),                           # Data emissao do titulo (DDMMAA)
          29 => Position.new(30, 2, true),                            # 1º instrucao
          30 => Position.new(31, 2, true),                            # 2º instrucao
          31 => Position.new(32, 13, true, "0"),                      # Valor por dia de atraso
          32 => Position.new(33, 6, false),                           # Data Limite para Concessao Desconto (DDMMAA)
          33 => Position.new(34, 13, true, "0"),                      # Valor desconto
          34 => Position.new(35, 13, true, "0"),                      # Valor IOF
          35 => Position.new(36, 13, true, "0"),                      # Valor Abatimento
          36 => Position.new(37, 2, false),                           # Tipo de inscrição (1:CPF, 2: CNPJ)
          37 => Position.new(38, 14, true),                           # Numero da inscricao
          38 => Position.new(39, 40, false),                          # Nome do sacado
          39 => Position.new(40, 40, false),                          # End. do sacado
          40 => Position.new(41, 12, false),                          # 1º mensagem
          41 => Position.new(42, 8, false),                           # CEP do sacado
          42 => Position.new(43, 60, false),                          # Sacador/Avalista ou 2º mensagem
          43 => Position.new(44, 6, true)                             # Numero sequencial

        })
      end

      def set_values params
        set_identificacao_empresa   params[:identificacao_empresa]
        set_nosso_numero            params[:nosso_numero]
        set_codigo_banco_debitado   params[:codigo_banco_debitado]
        set_reservado_1
        set_indicador_rateio
        set_enderecamento
        set_reservado_2
        set_numero_documento       params[:numero_documento]
        set_vencimento_titulo      params[:vencimento]
        set_valor_titulo           params[:valor]
        set_data_emissao_titulo    Date.today
        set_1_instrucao
        set_2_instrucao
        set_valor_por_atraso
        set_id_inscricao_sacado     params[:tipo_inscricao]
        set_numero_inscricao_sacado params[:numero_inscricao]
        set_nome_sacado             params[:nome_sacado]
        set_endereco                params[:endereco]
        set_1_mensagem
        set_cep                     params[:cep]
        set_2_mensagem
        set_sequencial              params[:sequencial]
      end

      def is_valid?
        get_identificacao_empresa.length > 0   and
        get_nosso_numero.length > 0            and
        get_numero_documento.length > 0        and
        get_sequencial.length > 0
      end

    end
  end

  module Retorno
    class Detalhe < FormatSection
      include V08DetalheCommons

      def initialize
        @section = Section.new({
          0 => Position.new(1, 1, false, "1", true),                 # Código do Registro
          1 => Position.new(2, 2, true),                              # Tipo de inscrição da empresa (01:CPF, 02: CNPJ)
          2 => Position.new(3, 14, true),                             # Numero da inscricao da empresa
          3 => Position.new(4, 3, false),                             # Reservado - Zeros
          4 => Position.new(5, 17, false),                            # Identificação da empresa (zero, carteira, agencia e CC )
          5 => Position.new(6, 25, false),                            # Numero controle do participante
          6 => Position.new(7, 8, true),                              # Reservado - Zeros
          7 => Position.new(8, 12, false),                            # Identificação do titulo no banco
          8 => Position.new(9, 10, false),                            # Reservado - Zeros
          9 => Position.new(10, 12, false),                           # Reservado - Zeros
          10 => Position.new(11, 1, false),                           # Indicador Rateio
          11 => Position.new(12, 2, false),                           # Reservado - Zeros
          12 => Position.new(13, 1, true),                            # Carteira
          13 => Position.new(14, 2, true),                            # Id da Ocorrencia
          14 => Position.new(15, 6, false),                           # Data da Ocorrencia
          15 => Position.new(16, 10, false),                          # Numero Documento
          16 => Position.new(17, 20, false),                          # Id do titulo no banco
          17 => Position.new(18, 6, false),                           # Data vencimento do titulo (DDMMAA)
          18 => Position.new(19, 13, true),                           # Valor do titulo
          19 => Position.new(20, 3, false),                           # Banco cobrador
          20 => Position.new(21, 5, false),                           # Agencia Cobradora
          21 => Position.new(22, 2, false),                           # Especie de titulo - branco
          22 => Position.new(23, 13, false),                          # Valor despesa das ocorrencias
          23 => Position.new(24, 13, false),                          # Outras despesas
          24 => Position.new(25, 13, false),                          # Juros operação em atraso
          25 => Position.new(26, 13, false),                          # IOF devido
          26 => Position.new(27, 13, false),                          # Valor abatimento
          27 => Position.new(28, 13, false),                          # Valor desconto concedido
          28 => Position.new(29, 13, false),                          # Valor pago
          29 => Position.new(30, 13, false),                          # Juros de mora
          30 => Position.new(31, 13, false),                          # Outros creditos
          31 => Position.new(32, 2, false),                           # Reservado - branco
          32 => Position.new(33, 1, false),                           # Motivo do codigo de ocorrencia
          33 => Position.new(34, 6, false),                           # Data do credito
          34 => Position.new(35, 3, false),                           # Origem pagamento
          35 => Position.new(36, 10, false),                          # Reservado - brancos
          36 => Position.new(37, 4, false),                           # Codigo do banco - quando cheque
          37 => Position.new(38, 10, false),                          # Motivo das rejeicoes
          38 => Position.new(39, 40, false),                          # Reservado - brancos
          39 => Position.new(40, 2, false),                           # numero do cartorio
          40 => Position.new(41, 10, false),                          # numero do protocolo
          41 => Position.new(42, 14, false),                          # Reservado - brancos
          42 => Position.new(43, 6, true)                             # Numero sequencial
        })
      end

      def process_section file
        set_tipo_inscricao             file[1..2]
        set_numero_inscricao           file[3..16]
        set_zeros_1                    file[17..19]
        set_id_empresa                 file[20..36]
        set_controle_participante      file[37..61]
        set_zeros_2                    file[62..69]
        set_id_titulo                  file[70..81]
        set_zeros_3                    file[82..91]
        set_zeros_4                    file[93..103]
        set_indicador_rateio_retorno   file[104..104]
        set_zeros_5                    file[105..106]
        set_carteira                   file[107..107]
        set_ocorrencia_retorno         file[108..109]
        set_data_ocorrencia            file[110..115]
        set_numero_documento_retorno   file[116..125]
        set_id_titulo_2                file[126..145]
        set_data_titulo                file[146..151]
        set_valor_titulo_retorno       file[152..164]
        set_banco_cobrador_retorno     file[165..167]
        set_agencia_cobradora          file[168..172]
        set_especie_titulo_retorno     file[173..174]
        set_despesas                   file[175..187]
        set_outras_despesas            file[188..200]
        set_juros                      file[201..213]
        set_iof                        file[214..226]
        set_abatimento_concedido       file[227..239]
        set_desconto_concedido         file[240..252]
        set_valor_pago                 file[253..265]
        set_juros_mora                 file[266..278]
        set_outros_creditos            file[279..291]
        set_brancos_1                  file[292..293]
        set_motivo_ocorrencia          file[294..294]
        set_data_credito               file[295..300]
        set_origem_pagamento           file[301..303]
        set_brancos_2                  file[304..313]
        set_codigo_banco_retorno       file[314..317]
        set_motivo_rejeicoes           file[318..327]
        set_brancos_3                  file[328..367]
        set_numero_cartorio            file[368..369]
        set_numero_protocolo           file[370..379]
        set_brancos_4                  file[380..393]
        set_sequencial_retorno         file[394..399]
      end

      def is_valid?
        get_tipo_inscricao.length > 0                and
        get_numero_inscricao.length > 0              and
        # get_controle_participante.length > 0         and
        # get_numero_documento_retorno.length > 0      and
        get_ocorrencia.length > 0                    and
        get_sequencial_retorno.length > 0
      end

    end
  end
end


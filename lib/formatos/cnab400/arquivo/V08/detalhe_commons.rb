require_relative '../parser_code_message'
module V08DetalheCommons
  include ParserCodeMessage


  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Getters
  def get_agencia
    self.get_section_value(1)
  end

  def get_identificacao_empresa
    self.get_section_value(6)
  end

  def get_nosso_numero
    self.get_section_value(7)
  end

  def get_numero_documento
    self.get_section_value(21)
  end

  def get_vencimento_titulo
    self.get_section_value(22)
  end

  def get_valor
    self.get_section_value(28).to_i
  end

  def get_nome_sacado
    self.get_section_value(38)
  end

  def get_endereco
    self.get_section_value(39)
  end

  def get_sequencial
    self.get_section_value(43)
  end

  def get_tipo_inscricao
    self.get_section_value(1)
  end

  def get_numero_inscricao
    self.get_section_value(2)
  end

  def get_controle_participante
    self.get_section_value(5)
  end

  def get_ocorrencia
    self.get_section_value(13)
  end

  def get_numero_documento_retorno
    self.get_section_value(15)
  end

  def get_data_credito
    self.get_section_value(33)
  end

  def get_motivo_rejeicao
    self.get_section_value(37)
  end

  def get_sequencial_retorno
    self.get_section_value(42)
  end

  def get_mensagem_retorno
    get_mensagem_ocorrencia get_ocorrencia
  end

  def get_mensagem_erro
    get_motivo_ocorrencia get_ocorrencia, get_motivo_rejeicao
  end


  #-------------------------------------------------------------------
  #-------------------------------------------------------------------
  # Setters
  def set_agencia valor = ""
    self.set_section_value(1, valor)
  end

  def set_tipo_inscricao valor = ""
    self.set_section_value(1, valor)
  end

  def set_dv_agencia valor = ""
    self.set_section_value(2, valor)
  end

  def set_numero_inscricao valor = ""
    self.set_section_value(2, valor)
  end

  def set_razao_conta_corrente valor = ""
    self.set_section_value(3, valor)
  end

  def set_zeros_1 valor = ""
    self.set_section_value(3, valor)
  end

  def set_conta_corrente valor = ""
    self.set_section_value(4, valor)
  end

  def set_id_empresa valor
    self.set_section_value(4, valor)
  end

  def set_dv_conta_corrente valor = ""
    self.set_section_value(5, valor)
  end

  def set_controle_participante valor
    self.set_section_value(5, valor)
  end

  def set_identificacao_empresa valor = ""
    self.set_section_value(6, valor)
  end

  def set_zeros_2 valor
    self.set_section_value(6, valor)
  end

  def set_nosso_numero valor = ""
    self.set_section_value(7, valor)
  end

  def set_id_titulo valor
    self.set_section_value(7, valor)
  end

  def set_codigo_banco_debitado valor = ""
    self.set_section_value(8, valor)
  end

  def set_zeros_3 valor
    self.set_section_value(8, valor)
  end

  def set_campo_multa valor = "0"
    self.set_section_value(9, valor)
  end

  def set_zeros_4 valor
    self.set_section_value(9, valor)
  end

  def set_percentual_multa valor = "0"
    self.set_section_value(10, valor)
  end

  def set_indicador_rateio_retorno valor
    self.set_section_value(10, valor)
  end

  def set_identificacao_titulo valor = "0"
    self.set_section_value(11, valor)
  end

  def set_zeros_5 valor
    self.set_section_value(11, valor)
  end

  def set_digito_conferencia valor = "0"
    self.set_section_value(12, valor)
  end

  def set_carteira valor
    self.set_section_value(12, valor)
  end

  def set_desconto_bonificacao valor = "0"
    self.set_section_value(13, valor)
  end

  def set_ocorrencia_retorno valor
    self.set_section_value(13, valor)
  end

  def set_condicao_emissao valor
    self.set_section_value(14, valor)
  end

  def set_data_ocorrencia valor
    self.set_section_value(14, valor)
  end

  def set_id_boleto_debito valor = " "
    self.set_section_value(15, valor)
  end

  def set_numero_documento_retorno valor
    self.set_section_value(15, valor)
  end

  def set_reservado_1 valor = " "
    self.set_section_value(16, valor)
  end

  def set_id_titulo_2 valor
    self.set_section_value(16, valor)
  end

  def set_indicador_rateio valor = ""
    self.set_section_value(17, valor)
  end

  def set_data_titulo valor = ""
    self.set_section_value(17, valor)
  end

  def set_enderecamento valor = ""
    self.set_section_value(18, valor)
  end

  def set_valor_titulo_retorno valor
    self.set_section_value(18, valor)
  end

  def set_reservado_2 valor = " "
    self.set_section_value(19, valor)
  end

  def set_banco_cobrador_retorno valor
    self.set_section_value(19, valor)
  end

  def set_indicador_ocorrencia valor
    self.set_section_value(20, valor)
  end

  def set_agencia_cobradora valor
    self.set_section_value(20, valor)
  end

  def set_numero_documento valor = ""
    self.set_section_value(21, valor)
  end

  def set_especie_titulo_retorno valor
    self.set_section_value(21, valor)
  end

  def set_vencimento_titulo valor
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(22, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_despesas valor
    self.set_section_value(22, valor)
  end

  def set_valor_titulo valor
    self.set_section_value(23, valor)
  end

  def set_outras_despesas valor
    self.set_section_value(23, valor)
  end

  def set_banco_cobrador valor = "0"
    self.set_section_value(24, valor)
  end

  def set_juros valor
    self.set_section_value(24, valor)
  end

  def set_agencia_depositaria  valor = "0"
    self.set_section_value(25, valor)
  end

  def set_iof valor
    self.set_section_value(25, valor)
  end

  def set_especie_titulo valor = ""
    self.set_section_value(26, valor)
  end

  def set_abatimento_concedido valor = ""
    self.set_section_value(26, valor)
  end

  def set_identificacao valor = ""
    self.set_section_value(27, valor)
  end

  def set_desconto_concedido valor = ""
    self.set_section_value(27, valor)
  end

  def set_data_emissao_titulo valor
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(28, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_valor_pago valor = ""
    self.set_section_value(28, valor)
  end

  def set_1_instrucao valor = "0"
    self.set_section_value(29, valor)
  end

  def set_juros_mora valor
    self.set_section_value(29, valor)
  end

  def set_2_instrucao valor = "0"
    self.set_section_value(30, valor)
  end

  def set_outros_creditos valor
    self.set_section_value(30, valor)
  end

  def set_valor_por_atraso valor = "0"
    self.set_section_value(31, valor)
  end

  def set_brancos_1 valor
    self.set_section_value(31, valor)
  end

  def set_data_limite_desconto valor = ""
    begin
      valor = Date.strptime(valor, "%d%m%Y") if valor.is_a?(String)
      real_data = valor.strftime("%d%m%y")

      if real_data.length == 6
        self.set_section_value(32, real_data)
      else
        raise "tamanho de data incorreto"
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{valor}"
    end
  end

  def set_motivo_ocorrencia valor
    self.set_section_value(32, valor)
  end

  def set_valor_desconto valor = "0"
    self.set_section_value(33, valor)
  end

  def set_data_credito valor = ""
    self.set_section_value(33, valor)
  end

  def set_valor_iof valor = "0"
    self.set_section_value(34, valor)
  end

  def set_origem_pagamento valor = ""
    self.set_section_value(34, valor)
  end

  def set_valor_abatimento valor
    self.set_section_value(35, valor)
  end

  def set_brancos_2 valor
    self.set_section_value(35, valor)
  end

  def set_id_inscricao_sacado valor = ""
    self.set_section_value(36, valor)
  end

  def set_codigo_banco_retorno valor = ""
    self.set_section_value(36, valor)
  end

  def set_numero_inscricao_sacado valor = ""
    self.set_section_value(37, valor)
  end

  def set_motivo_rejeicoes valor = ""
    self.set_section_value(37, valor)
  end

  def set_nome_sacado valor = ""
    self.set_section_value(38, valor)
  end

  def set_brancos_3 valor = ""
    self.set_section_value(38, valor)
  end

  def set_endereco valor = ""
    self.set_section_value(39, valor)
  end

  def set_numero_cartorio valor = ""
    self.set_section_value(39, valor)
  end

  def set_1_mensagem valor = ""
    self.set_section_value(40, valor)
  end

  def set_numero_protocolo valor = ""
    self.set_section_value(40, valor)
  end

  def set_cep valor = ""
    self.set_section_value(41, valor)
  end

  def set_brancos_4 valor = ""
    self.set_section_value(41, valor)
  end

  def set_2_mensagem valor = ""
    self.set_section_value(42, valor)
  end

  def set_sequencial_retorno valor = ""
    self.set_section_value(42, valor)
  end

  def set_sequencial valor = ""
    self.set_section_value(43, valor)
  end
end
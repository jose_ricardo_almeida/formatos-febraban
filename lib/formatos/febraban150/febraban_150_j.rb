
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Confirmação de Processamento - Empresa/Banco
class Febraban150J < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, true)

    @section = Section.new({
      0 => Position.new(1, 1, false, "J", true), # Código do Registro
      1 => Position.new(2, 6, true),             # Número Sequencial
      2 => Position.new(3, 8, true),             # Data de Geração
      3 => Position.new(4, 6, true),             # Total de Registros Processados
      4 => Position.new(5, 17, true),            # Valor total
      5 => Position.new(6, 8, true),             # Data de processamento
      6 => Position.new(7, 104, false)           # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_numero_sequencial  file[1..6]
    self.set_data_geracao       file[7..14]
    self.set_total_processados  file[15..20]
    self.set_valor_total        file[21..37]
    self.set_data_processamento file[38..45]
    self.set_reservado          file[46..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_numero_sequencial > 0     and
              !self.get_data_geracao.nil?        and
              self.get_total_processados >= 0    and
              self.get_valor_total >= 0          and
              !self.get_data_processamento.nil?)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_numero_sequencial
    self.set_section_value(1).to_i
  end

  def get_data_geracao
    data = self.set_section_value(2)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_total_processados
    self.set_section_value(3).to_i
  end

  def get_valor_total
    self.set_section_value(4).to_i
  end

  def get_data_processamento
    data = self.set_section_value(5)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_numero_sequencial numero
    numero = numero.to_i

    if numero > 0
      self.set_section_value(1, numero)
    else
      raise "#{self.get_id}: Número Sequencial deve ser positivo e maior que 0
              Valor: #{numero}"
    end
  end

  def set_data_geracao data
      data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
      real_data = data.strftime("%Y%m%d")

      if real_data.length == 8
      self.set_section_value(2, data)
    else
      raise "#{self.get_id}: Data de Geração não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_total_processados total
    total = total.to_i

    if total >= 0
      self.set_section_value(3, total)
    else
      raise "#{self.get_id}: Total de Arquivos Processados deve ser positivo ou 0
              Valor: #{total}"
    end
  end

  def set_valor_total valor
    valor = valor.to_i

    if valor >= 0
      self.set_section_value(4, valor)
    else
      raise "#{self.get_id}: Valor Total dos Arquivos Processados deve ser positivo ou 0
              Valor: #{valor}"
    end
  end

  def set_data_processamento data
    data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
    real_data = data.strftime("%Y%m%d")

    if real_data.length == 8
      self.set_section_value(5, real_data)
    else
      raise "#{self.get_id}: Data de Processamento não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(6, reservado)
  end
end

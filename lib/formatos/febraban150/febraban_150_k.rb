
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Lei 10833, Cobrança não-cumulativa COFINS  - Empresa
# http://www.planalto.gov.br/ccivil_03/leis/2003/L10.833compilado.htm
class Febraban150K < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, false, true)

    @section = Section.new({
      0 => Position.new(1, 1, false, "K", true), # Código do Registro
      1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
      2 => Position.new(3, 4, false),            # Agência para Débito
      3 => Position.new(4, 14, false),           # Identificação do Cliente Banco
      4 => Position.new(5, 2, true),             # Tipo de Tratamento
      5 => Position.new(6, 15, true),            # Valor Débito
      6 => Position.new(7, 4, false),            # Código Receita
      7 => Position.new(8, 1, true),             # Tipo de Identificação (1-CPF, 2-CNPJ)
      8 => Position.new(9, 15, true),            # Identificação
      9 => Position.new(10, 69, false)          # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_id_cliente_empresa file[1..25]
    self.set_agencia_debito     file[26..29]
    self.set_id_cliente_banco   file[30..43]
    self.set_tipo_tratamento    file[41..45]
    self.set_valor_debitado     file[46..60]
    self.set_codigo_receita     file[61..64]
    self.set_tipo_id_cliente    file[65..65]
    self.set_id_cliente         file[66..80]
    self.set_reservado          file[81..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_id_cliente_empresa.length > 0 and
              self.get_agencia_debito.length > 0     and
              self.get_id_cliente_banco.length > 0   and
              self.get_tipo_tratamento > 0           and
              self.get_valor_debitado > 0            and
              self.get_codigo_receita.length > 0     and
              (self.get_tipo_id_cliente == 1 or
               self.get_tipo_id_cliente == 2)        and
              self.get_id_cliente.length == 15)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_id_cliente_empresa
    self.get_section_value(1)
  end

  def get_agencia_debito
    self.get_section_value(2)
  end

  def get_id_cliente_banco
    self.get_section_value(3)
  end

  def get_tipo_tratamento
    self.get_section_value(4).to_i
  end

  def get_valor_debitado
    self.get_section_value(5).to_i
  end

  def get_codigo_receita
    self.get_section_value(6)
  end

  def get_tipo_id_cliente
    self.get_section_value(7).to_i
  end

  def get_id_cliente
    self.get_section_value(8)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_id_cliente_empresa id_cliente_empresa
    id_cliente_empresa = id_cliente_empresa.to_s

    if id_cliente_empresa.length > 0
      self.set_section_value(1, id_cliente_empresa)
    else
      raise "#{self.get_id}: Identificação do Cliente da Empresa não pode estar vazio
              Valor: #{id_cliente_empresa}"
    end
  end

  def set_agencia_debito agencia_debito
    agencia_debito = agencia_debito.to_s

    if agencia_debito.length > 0
      self.set_section_value(2, agencia_debito)
    else
      raise "#{self.get_id}: Agência do Débito não pode estar vazia
              Valor: #{agencia_debito}"
    end
  end

  def set_id_cliente_banco id_cliente_banco
    id_cliente_banco = id_cliente_banco.to_s

    if id_cliente_banco.length > 0
      self.set_section_value(3, id_cliente_banco)
    else
      raise "#{self.get_id}: Identificação do Cliente do Banco não pode estar vazio
              Valor: #{id_cliente_banco}"
    end
  end

  def set_tipo_tratamento tratamento
    tratamento = tratamento.to_i

    if tratamento > 0
      self.set_section_value(4, tratamento)
    else
      raise "#{self.get_id}: Tipo de Tratamento não pode estar vazio
              Valor: #{tratamento}"
    end
  end

  def set_valor_debitado valor
    valor = valor.to_i

    if valor > 0
      self.set_section_value(5, valor)
    else
      raise "#{self.get_id}: Valor a ser Debitado deve ser positivo ou 0
              Valor: #{valor}"
    end
  end

  def set_codigo_receita codigo
    codigo = codigo.to_s

    if codigo.length > 0
      self.set_section_value(6, codigo)
    else
      raise "#{self.get_id}: Valor a ser Debitado deve ser positivo ou 0
              Valor: #{codigo}"
    end
  end

  def set_tipo_id_cliente tipo_id_cliente
    tipo_id_cliente = tipo_id_cliente.to_i

    if tipo_id_cliente == 1 or tipo_id_cliente == 2
      self.set_section_value(7, tipo_id_cliente)
    else
      raise "#{self.get_id}: Tipo de Identificação do Cliente deve ser:
              1 - CNPJ; 2 - CPF
              Valor: #{tipo_id_cliente}"
    end
  end

  def set_id_cliente id_cliente
    id_cliente = id_cliente.to_s
    id_cliente.to_i

    if id_cliente.length >= 13 or id_cliente.length == 0
      self.set_section_value(9, id_cliente)
    else
      raise "#{self.get_id}: Identificação do Cliente deve ser CPF ou CNPJ:
              CNPJ: 999999999 = Número, 9999 = Filial, e 99 = DV
              CPF: 0000999999999 = Número, 99 = DV
              Valor: #{id_cliente}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(9, reservado)
  end
end

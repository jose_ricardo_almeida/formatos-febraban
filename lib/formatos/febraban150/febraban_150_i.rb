
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Incentivo de Débito Automático - Empresa
class Febraban150I < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, false, true)

    @section = Section.new({
      0 => Position.new(1, 1, false, "I", true), # Código do Registro
      1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
      2 => Position.new(3, 1, false),            # Tipo de Identificação (1-CPF, 2-CNPJ)
      3 => Position.new(4, 14, true),            # Identificação
      4 => Position.new(5, 40, false),           # Nome do Consumidor
      5 => Position.new(6, 30, false),           # Cidade do Consumidor
      6 => Position.new(7, 2, false),            # Estado do Consumidor
      7 => Position.new(8, 37, false)            # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_id_cliente_empresa file[1..25]
    self.set_tipo_id_cliente    file[26..26]
    self.set_id_cliente         file[27..40]
    self.set_nome               file[41..80]
    self.set_cidade             file[81..110]
    self.set_estado             file[111..112]
    self.set_reservado          file[113..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_id_cliente_empresa.length > 0 and
              (self.get_tipo_id_cliente == 1 or
               self.get_tipo_id_cliente == 2)        and
              self.get_id_cliente.length == 15       and
              self.get_nome.length > 0               and
              self.get_cidade.length > 0             and
              self.get_estado.length > 0)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_id_cliente_empresa
    self.get_section_value(1)
  end

  def get_tipo_id_cliente
    self.get_section_value(2).to_i
  end

  def get_id_cliente
    self.get_section_value(3)
  end

  def get_nome
    self.get_section_value(4)
  end

  def get_cidade
    self.get_section_value(5)
  end

  def get_estado
    self.get_section_value(6)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_id_cliente_empresa id_cliente_empresa
    id_cliente_empresa = id_cliente_empresa.to_s

    if id_cliente_empresa.length > 0
      self.set_section_value(1, id_cliente_empresa)
    else
      raise "#{self.get_id}: Identificação do Cliente da Empresa não pode estar vazio
              Valor: #{id_cliente_empresa}"
    end
  end

  def set_tipo_id_cliente tipo_id_cliente
    tipo_id_cliente = tipo_id_cliente.to_i

    if tipo_id_cliente == 1 or tipo_id_cliente == 2
      self.set_section_value(2, tipo_id_cliente)
    else
      raise "#{self.get_id}: Tipo de Identificação do Cliente deve ser:
              1 - CNPJ; 2 - CPF
              Valor: #{tipo_id_cliente}"
    end
  end

  def set_id_cliente id_cliente
    id_cliente = id_cliente.to_s
    id_cliente.to_i

    if id_cliente.length >= 13 or id_cliente.length == 0
      self.set_section_value(9, id_cliente)
    else
      raise "#{self.get_id}: Identificação do Cliente deve ser CPF ou CNPJ:
              CNPJ: 999999999 = Número, 9999 = Filial, e 99 = DV
              CPF: 0000999999999 = Número, 99 = DV
              Valor: #{id_cliente}"
    end
  end

  def set_nome nome
    nome = nome.to_s

    if nome.length > 0
      self.set_section_value(4, nome)
    else
      raise "#{self.get_id}: Nome do Cliente não pode estar em branco
              Valor: #{nome}"
    end
  end

  def set_cidade cidade
    cidade = cidade.to_s

    if cidade.length > 0
      self.set_section_value(5, cidade)
    else
      raise "#{self.get_id}: Cidade do Cliente não pode estar em branco
              Valor: #{cidade}"
    end
  end

  def set_estado estado
    estado = estado.to_s

    if estado.length > 0
      self.set_section_value(6, estado)
    else
      raise "#{self.get_id}: Estado do Cliente não pode estar em branco
              Valor: #{estado}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(7, reservado)
  end
end

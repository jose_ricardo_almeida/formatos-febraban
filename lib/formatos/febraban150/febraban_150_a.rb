
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Header - Empresa e Banco
class Febraban150A < FormatSection
  require 'date'

  ID_SERVICO = "DÉBITO AUTOMÁTICO"

  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, true, false)

    @versao = versao

    case @versao
      when '04'
        self.monta_versao_04
      when '05'
        self.monta_versao_05
      else
        raise "Seção A: Versão não suportada: #{@versao}"
    end
  end

  protected
  def monta_versao_04
    @section = Section.new({
      0 => Position.new(1, 1, false, 'A', true),                  # Código do Registro
      1 => Position.new(2, 1, true),                              # Código Remessa (1-Remessa, 2-Retorno)
      2 => Position.new(3, 20, false),                            # Código de ID enviado pelo Banco
      3 => Position.new(4, 20, false),                            # Nome da Empresa
      4 => Position.new(5, 3, true),                              # Código do Banco
      5 => Position.new(6, 20, false),                            # Nome do Banco
      6 => Position.new(7, 8, true),                              # Data de Geração de Arquivo
      7 => Position.new(8, 6, true),                              # Número Sequencial do Arquivo
      8 => Position.new(9, 2, true, @versao),                     # Versão do Layout (5, desde 05.05.2008)
      9 => Position.new(10, 17, false, Febraban150A::ID_SERVICO), # Identificação ("DÉBITO AUTOMÁTICO")
      10 => Position.new(11, 52, false),                          # Reservado pelo Sistema
    })
  end

  def monta_versao_05
    @section = Section.new({
      0 => Position.new(1, 1, false,'A', true),                  # Código do Registro
      1 => Position.new(2, 1, true),                              # Código Remessa (1-Remessa, 2-Retorno)
      2 => Position.new(3, 20, false),                            # Código de ID enviado pelo Banco
      3 => Position.new(4, 20, false),                            # Nome da Empresa
      4 => Position.new(5, 3, true),                              # Código do Banco
      5 => Position.new(6, 20, false),                            # Nome do Banco
      6 => Position.new(7, 8, true),                              # Data de Geração de Arquivo
      7 => Position.new(8, 6, true),                              # Número Sequencial do Arquivo
      8 => Position.new(9, 2, true, @versao),                     # Versão do Layout (5, desde 05.05.2008)
      9 => Position.new(10, 17, false, Febraban150A::ID_SERVICO), # Identificação ("DÉBITO AUTOMÁTICO")
      10 => Position.new(11, 52, false),                          # Reservado pelo Sistema
    })
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  public
  def process_section(file)
    case @versao
      when '04'
        self.processa_arquivo_04(file)
      when '05'
        self.processa_arquivo_05(file)
    end
  end

  protected
  def processa_arquivo_04(file)
    self.set_codigo_remessa        file[1..1]
    self.set_codigo_convenio       file[2..21]
    self.set_nome_empresa          file[22..41]
    self.set_codigo_banco          file[42..44]
    self.set_nome_banco            file[45..64]
    self.set_data_geracao          file[65..72]
    self.set_numero_sequencial     file[73..78]
    self.set_versao_layout         file[79..80]
    self.set_identificacao_servico file[81..99]
    self.set_reservado             file[98..149]
  end

  def processa_arquivo_05(file)
    self.set_codigo_remessa        file[1..1]
    self.set_codigo_convenio       file[2..21]
    self.set_nome_empresa          file[22..41]
    self.set_codigo_banco          file[42..44]
    self.set_nome_banco            file[45..64]
    self.set_data_geracao          file[65..72]
    self.set_numero_sequencial     file[73..78]
    self.set_versao_layout         file[79..80]
    self.set_identificacao_servico file[81..99]
    self.set_reservado             file[98..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  public
  def is_valid?
    case @versao
      when '04'
        self.is_valid_04?
      when '05'
        self.is_valid_05?
    end
  end

  protected
  def is_valid_04?
    result = (self.get_codigo_remessa > 0         and
              self.get_codigo_convenio.length > 0 and
              self.get_nome_empresa.length > 0    and
              self.get_codigo_banco > 0           and
              self.get_nome_banco.length > 0      and
              !self.get_data_geracao.nil?         and
              self.get_numero_sequencial > 0      and
              self.get_versao_layout > 0          and
              self.get_identificacao_servico.length > 0)
  end

  def is_valid_05?
    result = (self.get_codigo_remessa > 0         and
              self.get_codigo_convenio.length > 0 and
              self.get_nome_empresa.length > 0    and
              self.get_codigo_banco > 0           and
              self.get_nome_banco.length > 0      and
              !self.get_data_geracao.nil?         and
              self.get_numero_sequencial > 0      and
              self.get_versao_layout > 0          and
              self.get_identificacao_servico.length > 0)
  end


#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
  def get_codigo_remessa
    self.get_section_value(1).to_i
  end

  def get_codigo_convenio
    self.get_section_value(2)
  end

  def get_nome_empresa
    self.get_section_value(3)
  end

  def get_codigo_banco
    self.get_section_value(4).to_i
  end

  def get_nome_banco
    self.get_section_value(5)
  end

  def get_data_geracao
    data = self.get_section_value(6)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_numero_sequencial
    data = self.get_section_value(7).to_i
  end

  def get_versao_layout
    data = self.get_section_value(8).to_i
  end

  def get_identificacao_servico
    data = self.get_section_value(9)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  public
  def set_codigo_remessa code
    code = code.to_i

    if code == 1 or code == 2
      self.set_section_value(1, code)
    else
      raise "#{get_id}: 1 = Remessa - Enviado pela Empresa para o Banco
              2 = Retorno - Enviado pelo Banco para a Empresa
              Valor: #{code}"
    end
  end

  def set_codigo_convenio code
    code = code.to_s

    if !code.nil? and
      (code.length > 0 and code.length <= 20)

      self.set_section_value(2, code)
    else
      raise "#{get_id}: Código atribuído pelo Banco, para seu controle interno.
              Este código será informado à Empresa, pelo Banco, antes
              da implantação do serviço de débito automático.
              Valor: #{code}"
    end
  end

  def set_nome_empresa nome
    nome = nome.to_s

    if nome.to_s.length > 0
      self.set_section_value(3, nome)
    else
      raise "#{get_id}: Nome da Empresa não pode estar vazio"
    end
  end

  def set_codigo_banco codigo
    codigo = codigo.to_i

    if codigo > 0
      self.set_section_value(4, codigo)
    else
      raise "#{get_id}: Código do Banco não pode estar vazio"
    end
  end

  def set_nome_banco nome
    nome = nome.to_s

    if nome.to_s.length > 0
      self.set_section_value(5, nome)
    else
      raise "#{get_id}: Nome do Banco não pode estar vazio"
    end
  end

  def set_data_geracao data
    begin
      data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
      real_data = data.strftime("%Y%m%d")

      if real_data.length == 8
        self.set_section_value(6, real_data)
      else
        raise
      end

    rescue
      raise "#{get_id}: Data de Geração Inválida
              Valor: #{data}"
    end
  end

  def set_numero_sequencial numero
    numero = numero.to_i

    if numero > 0
      self.set_section_value(7, numero)
    else
      raise "#{get_id}: Número Sequencial deve ser positivo e maior que 0
              Valor: #{numero}"
    end
  end

  def set_versao_layout versao
    versao = versao.to_i

    if versao > 0
      self.set_section_value(8, versao)
    else
      raise "#{get_id}: Versão deve ser 05 (a partir de 05.05.2008)
              Valor: #{versao}"
    end
  end

  def set_identificacao_servico identificacao
    identificacao = identificacao.to_s

    if identificacao.length > 0
      self.set_section_value(9, identificacao)
    else
      raise "#{get_id}: Deve ser Débito Automático
              Valor: #{identificacao}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(10, reservado)
  end
end

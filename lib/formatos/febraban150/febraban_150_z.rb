
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Footer/Trailler - Empresa/Banco
class Febraban150Z < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, true, false)

    @versao = versao

    case versao
      when '04'
        self.monta_versao_04
      when '05'
        self.monta_versao_05
      else
        raise "Seção Z: Versão não suportada: #{@versao}"
    end
  end

  protected
  def monta_versao_04
    @section = Section.new({
         0 => Position.new(1, 1, false, 'Z', true), # Código do Registro
         1 => Position.new(2, 6, true),             # Total de Registros no Arquivo
         2 => Position.new(3, 17, true),            # Valor Total dos Registros
         3 => Position.new(4, 126, false)           # Reservado pelo Sistema
     })
  end

  def monta_versao_05
    @section = Section.new({
        0 => Position.new(1, 1, false, 'Z', true), # Código do Registro
        1 => Position.new(2, 6, true),             # Total de Registros no Arquivo
        2 => Position.new(3, 17, true),            # Valor Total dos Registros
        3 => Position.new(4, 126, false)           # Reservado pelo Sistema
    })
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  public
  def process_section(file)
    case @versao
      when '04'
        self.processa_arquivo_04(file)
      when '05'
        self.processa_arquivo_05(file)
    end
  end

  protected
  def processa_arquivo_04(file)
    self.set_total_registros file[1..6]
    self.set_valor_total     file[7..23]
    self.set_reservado       file[24..149]
  end

  def processa_arquivo_05(file)
    self.set_total_registros file[1..6]
    self.set_valor_total     file[7..23]
    self.set_reservado       file[24..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  public
  def is_valid?
    case @versao
      when '04'
        self.is_valid_04?
      when '05'
        self.is_valid_05?
    end
  end

  protected
  def is_valid_04?
    result = (self.get_total_registros > 0 and
              self.get_valor_total > 0)
  end

  def is_valid_05?
    result = (self.get_total_registros > 0 and
              self.get_valor_total > 0)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
  def get_total_registros
    self.get_section_value(1).to_i
  end

  def get_valor_total
    self.get_section_value(2).to_i
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  public
  def set_total_registros registros
    registros = registros.to_i

    if registros > 0
      self.set_section_value(1, registros)
    else
      raise "#{get_id}: A quantidade de registros deve ser positiva e maior que 0
              Valor: #{registros}"
    end
  end

  def set_valor_total valor
    valor = valor.to_i

    if valor > 0
      self.set_section_value(2, valor)
    else
      raise "#{get_id}: A quantidade de registros deve ser positiva e maior que 0
              Valor: #{valor}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(3, reservado)
  end
end

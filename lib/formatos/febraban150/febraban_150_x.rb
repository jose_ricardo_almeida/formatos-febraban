
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Relação de Agências - Banco
class Febraban150X < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, false, false)

    @section = Section.new({
      0 => Position.new(1, 1, false, "X", true), # Código do Registro
      1 => Position.new(2, 4, false),            # Código da Agência
      2 => Position.new(3, 30, false),           # Nome da Agência
      3 => Position.new(4, 30, false),           # Endereço da Agência
      4 => Position.new(5, 5, false),            # Número
      5 => Position.new(6, 5, false),            # Código do CEP
      6 => Position.new(7, 3, false),            # Sufixo do CEP
      7 => Position.new(8, 20, false),           # Nome da Cidade
      8 => Position.new(9, 2, false),            # Sigla do Estado
      9 => Position.new(10, 1, false),          # Situação da Agência (A, B)
      10 => Position.new(11, 49, false)          # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_codigo_agencia   file[1..4]
    self.set_nome_agencia     file[5..34]
    self.set_endereco_agencia file[35..64]
    self.set_numero_agencia   file[65..69]
    self.set_codigo_cep       file[70..74]
    self.set_sufixo_cep       file[75..77]
    self.set_nome_cidade      file[78..97]
    self.set_estado           file[98..99]
    self.set_situacao_agencia file[100..100]
    self.set_reservado        file[101..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_codigo_agencia.length > 0   and
              self.get_nome_agencia.length > 0     and
              self.get_endereco_agencia.length > 0 and
              self.get_numero_agencia.length > 0   and
              self.get_codigo_cep.length == 5      and
              self.get_sufixo_cep.length == 3      and
              self.get_nome_cidade.length > 0      and
              self.get_estado.length == 2          and
              (self.get_situacao_agencia == "A" or
               self.get_situacao_agencia == "B"))
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_codigo_agencia
    self.set_section_value(1)
  end

  def get_nome_agencia
    self.set_section_value(2)
  end

  def get_endereco_agencia
    self.set_section_value(3)
  end

  def get_numero_agencia
    self.set_section_value(4)
  end

  def get_codigo_cep
    self.set_section_value(5)
  end

  def get_sufixo_cep
    self.set_section_value(6)
  end

  def get_nome_cidade
    self.set_section_value(7)
  end

  def get_estado
    self.set_section_value(8)
  end

  def get_situacao_agencia
    self.set_section_value(9)
  end

  def get_cep
    "#{self.get_codigo_cep}#{self.get_sufixo_cep}"
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_codigo_agencia codigo
    codigo = codigo.to_s

    if codigo.length > 0
      self.set_section_value(1, codigo)
    else
      raise "#{self.get_id}: Código da Agência deve estar preenchido
              Valor: #{codigo}"
    end
  end

  def set_nome_agencia nome
    nome = nome.to_s

    if nome.length > 0
      self.set_section_value(2, nome)
    else
      raise "#{self.get_id}: Nome da Agência deve estar preenchido
              Valor: #{nome}"
    end
  end

  def set_endereco_agencia endereco
    endereco = endereco.to_s

    if endereco.length > 0
      self.set_section_value(3, endereco)
    else
      raise "#{self.get_id}: Endereço da Agência deve estar preenchido
              Valor: #{endereco}"
    end
  end

  def set_numero_agencia numero
    numero = numero.to_s

    if numero.length > 0
      self.set_section_value(4, numero)
    else
      raise "#{self.get_id}: Número da Agência deve estar preenchido
              Valor: #{numero}"
    end
  end

  def set_codigo_cep codigo
    codigo = codigo.to_s

    if codigo.length == 5
      self.set_section_value(5, codigo)
    else
      raise "#{self.get_id}: Código do CEP da Agência deve ter 5 dígitos
              Valor: #{codigo}"
    end
  end

  def set_sufixo_cep codigo
    codigo = codigo.to_s

    if codigo.length == 3
      self.set_section_value(6, codigo)
    else
      raise "#{self.get_id}: Código do CEP da Agência deve ter 3 dígitos
              Valor: #{codigo}"
    end
  end

  def set_nome_cidade cidade
    cidade = cidade.to_s

    if cidade.length > 0
      self.set_section_value(7, cidade)
    else
      raise "#{self.get_id}: Cidade da Agência deve estar preenchido
              Valor: #{cidade}"
    end
  end

  def set_estado estado
    estado = estado.to_s

    if estado.length == 2
      self.set_section_value(8, estado)
    else
      raise "#{self.get_id}: Estado da Agência estar em siglas de 2 caracteres
              Valor: #{estado}"
    end
  end

  def set_situacao_agencia situacao
    situacao = situacao.to_s

    if situacao == "A" or situacao == "B"
      self.set_section_value(9, situacao)
    else
      raise "#{self.get_id}: Agência permite estados:
              A: Ativa
              B: Em regime de Encerramento
              Valor: #{situacao}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(10, reservado)
  end

  def set_cep codigo
    codigo = codigo.to_s

     self.set_codigo_cep(codigo[0..4])
     self.set_sufixo_cep(codigo[5..7])
  end
end

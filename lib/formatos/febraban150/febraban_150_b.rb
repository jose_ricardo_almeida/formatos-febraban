
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Cadastro Débito Automático - Banco
class Febraban150B < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, false)

    @section = Section.new({
      0 => Position.new(1, 1, false, "B", true), # Código do Registro
      1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
      2 => Position.new(3, 4, false),            # Agência para Débito
      3 => Position.new(4, 14, false),           # Identificação Cliente Banco
      4 => Position.new(5, 8, true),             # Data da Opção/Exclusão
      5 => Position.new(6, 97, false),           # Reservado pelo Sistema
      6 => Position.new(7, 1, true)              # Código de Movimento (1-Excluir, 2-Incluir)
    })
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_id_cliente_empresa  file[1..25]
    self.set_agencia_debito      file[26..29]
    self.set_id_cliente_banco    file[30..43]
    self.set_data_opcao_exclusao file[44..51]
    self.set_reservado           file[52..148]
    self.set_cod_movimento       file[149..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_id_cliente_empresa.length > 0 and
              self.get_agencia_debito.length > 0     and
              self.get_nome_empresa.length > 0       and
              !self.get_data_opcao_exclusao.nil?     and
              (self.get_cod_movimento == 0 or
               self.get_cod_movimento == 1))
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_id_cliente_empresa
    self.get_section_value(1)
  end

  def get_agencia_debito
    self.get_section_value(2)
  end

  def get_id_cliente_banco
    self.get_section_value(3)
  end

  def get_data_opcao_exclusao
    data = self.get_section_value(4)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_cod_movimento
    self.get_section_value(6).to_i
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_id_cliente_empresa id_cliente_empresa
    id_cliente_empresa = id_cliente_empresa.to_s

    if id_cliente_empresa.length > 0
      self.set_section_value(1, id_cliente_empresa)
    else
      raise "#{self.get_id}: Identificação do Cliente da Empresa não pode estar vazio
              Valor: #{id_cliente_empresa}"
    end
  end

  def set_agencia_debito agencia_debito
    agencia_debito = agencia_debito.to_s

    if agencia_debito.length > 0
      self.set_section_value(2, agencia_debito)
    else
      raise "#{self.get_id}: Agência do Débito não pode estar vazia
              Valor: #{agencia_debito}"
    end
  end

  def set_id_cliente_banco id_cliente_banco
    id_cliente_banco = id_cliente_banco.to_s

    if id_cliente_banco.length > 0
      self.set_section_value(3, id_cliente_banco)
    else
      raise "#{self.get_id}: Identificação do Cliente do Banco não pode estar vazio
              Valor: #{id_cliente_banco}"
    end
  end

  def set_data_opcao_exclusao data_opcao_exclusao
      data_opcao_exclusao = Date.strptime(data_opcao_exclusao, "%Y%m%d") if data_opcao_exclusao.is_a?(String)
      real_data = data_opcao_exclusao.strftime("%Y%m%d")

      if real_data.length == 8
      self.set_section_value(4, real_data)
    else
      raise "#{self.get_id}: Data de Opção/Exclusão não pode estar vazia
              Valor: #{data_opcao_exclusao}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(5, reservado)
  end

  def set_cod_movimento cod_movimento
    cod_movimento = cod_movimento.to_i

    if cod_movimento == 0 or cod_movimento == 1
      self.set_section_value(6, cod_movimento)
    else
      raise "#{self.get_id}: Código de Movimentação deve ser de Exclusão ou Inclusão:
              1 = Exclusão de optante pelo débito automático
              2 = Inclusão de optante pelo débito automático
              Valor: #{cod_movimento}"
    end
  end
end

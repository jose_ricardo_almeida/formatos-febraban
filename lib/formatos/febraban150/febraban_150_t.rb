
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Total de Clientes debitados - Banco
class Febraban150T < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, false)

    @section = Section.new({
      0 => Position.new(1, 1, false, "T", true), # Código do Registro
      1 => Position.new(2, 6, true),             # Total de Registros Debitados
      2 => Position.new(3, 17, true),            # Valor dos Registros Debitados
      3 => Position.new(4, 126, false)           # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_total_debitados       file[1..6]
    self.set_valor_total_debitados file[7..23]
    self.set_reservado             file[24..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_total_debitados >= 0 and
              self.get_valor_total_debitados >= 0 )
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_total_debitados
    self.set_section_value(1).to_i
  end

  def get_valor_total_debitados
    self.set_section_value(2).to_i
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_total_debitados valor
    valor = valor.to_i

    if valor >= 0
      self.set_section_value(1, valor)
    else
      raise "#{self.get_id}: Total dos Arquivos Debitados deve ser positivo ou 0
              Valor: #{valor}"
    end
  end

  def set_valor_total_debitados valor
    valor = valor.to_i

    if valor >= 0
      self.set_section_value(2, valor)
    else
      raise "#{self.get_id}: Valor Total dos Arquivos Debitados deve ser positivo ou 0
              Valor: #{valor}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(3, reservado)
  end
end

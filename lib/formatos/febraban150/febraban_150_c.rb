
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Ocorrências - Empresa
class Febraban150C < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, false, true)

    @section = Section.new({
      0 => Position.new(1, 1, false, "C", true), # Código do Registro
      1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
      2 => Position.new(3, 4, false),            # Agência para Débito
      3 => Position.new(4, 14, false),           # Identificação Cliente Banco
      4 => Position.new(5, 40, false),           # Ocorrência 1 - Motivo Recusa
      5 => Position.new(6, 40, false),           # Ocorrência 2 - Complemento
      6 => Position.new(7, 25, false),           # Reservado pelo Sistema
      7 => Position.new(8, 1, true)              # Código de Movimento (Seção B)
    })
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_id_cliente_empresa file[1..25]
    self.set_agencia_debito     file[26..29]
    self.set_id_cliente_banco   file[30..43]
    self.set_ocorrencia_1       file[44..83]
    self.set_ocorrencia_2       file[84..123]
    self.set_reservado          file[124..148]
    self.set_cod_movimento      file[149..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (self.get_id_cliente_empresa.length > 0 and
              self.get_agencia_debito.length > 0     and
              self.get_id_cliente_banco.length > 0   and
              self.get_ocorrencia_1.length > 0       and
              self.get_ocorrencia_2.length > 0       and
              (self.get_cod_movimento == 0 or
               self.get_cod_movimento == 1))
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_id_cliente_empresa
    self.get_section_value(1)
  end

  def get_agencia_debito
    self.get_section_value(2)
  end

  def get_id_cliente_banco
    self.get_section_value(3)
  end

  def get_ocorrencia_1
    self.get_section_value(4)
  end

  def get_ocorrencia_2
    self.get_section_value(5)
  end

  def get_cod_movimento
    self.get_section_value(7).to_i
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_id_cliente_empresa id_cliente_empresa
    id_cliente_empresa = id_cliente_empresa.to_s

    if id_cliente_empresa.length > 0
      self.set_section_value(1, id_cliente_empresa)
    else
      raise "#{self.get_id}: Identificação do Cliente da Empresa não pode estar vazio
              Valor: #{id_cliente_empresa}"
    end
  end

  def set_agencia_debito agencia_debito
    agencia_debito = agencia_debito.to_s

    if agencia_debito.length > 0
      self.set_section_value(2, agencia_debito)
    else
      raise "#{self.get_id}: Agência do Débito não pode estar vazia
              Valor: #{agencia_debito}"
    end
  end

  def set_id_cliente_banco id_cliente_banco
    id_cliente_banco = id_cliente_banco.to_s

    if id_cliente_banco.length > 0
      self.set_section_value(3, id_cliente_banco)
    else
      raise "#{self.get_id}: Identificação do Cliente do Banco não pode estar vazio
              Valor: #{id_cliente_banco}"
    end
  end

  def set_ocorrencia_1 ocorrencia
    ocorrencia = ocorrencia.to_s

    if ocorrencia.length > 0
      self.set_section_value(4, ocorrencia)
    else
      raise "#{self.get_id}: Motivo de Recusa não pode estar vazio
              Valor: #{ocorrencia}"
    end
  end

  def set_ocorrencia_2 ocorrencia
    ocorrencia = ocorrencia.to_s

    if ocorrencia.length > 0
      self.set_section_value(5, ocorrencia)
    else
      raise "#{self.get_id}: Motivo de Recusa não pode estar vazio
              Valor: #{ocorrencia}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(6, reservado)
  end

  def set_cod_movimento cod_movimento
    cod_movimento = cod_movimento.to_i

    if cod_movimento == 0 or cod_movimento == 1
      self.set_section_value(7, cod_movimento)
    else
      raise "#{self.get_id}: Código de Movimentação deve ser de Exclusão ou Inclusão:
              1 = Exclusão de optante pelo débito automático
              2 = Inclusão de optante pelo débito automático
              Valor: #{cod_movimento}"
    end
  end
end

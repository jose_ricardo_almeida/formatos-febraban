
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Cronograma de Faturamento Contas/Tributos - Empresa
class Febraban150L < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, false, true)

    @section = Section.new({
      0 => Position.new(1, 1, false, "L", true), # Código do Registro
      1 => Position.new(2, 9, true),             # Data do Faturamento das Contas
      2 => Position.new(3, 9, true),             # Data do Vencimento da Fatura
      3 => Position.new(4, 9, true),             # Data da Remessa do Arquivo
      4 => Position.new(5, 9, true),             # Data da Remessa das Contas
      5 => Position.new(6, 104, false)           # Reservado pelo Sistema
    }, true)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  def process_section file
    self.set_data_faturamento       file[1..8]
    self.set_data_vencimento_fatura file[9..16]
    self.set_data_remessa_banco     file[17..24]
    self.set_data_remessa_fisica    file[25..32]
    self.set_reservado              file[33..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  def is_valid?
    result = (!self.get_id_cliente_empresa.nil? and
              !self.get_agencia_debito.nil?     and
              !self.get_id_cliente_banco.nil?   and
              !self.get_id_cliente.nil?)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  def get_data_faturamento
    data = self.set_section_value(1)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_data_vencimento_fatura
    data = self.set_section_value(2)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_data_remessa_banco
    data = self.set_section_value(3)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_data_remessa_fisica
    data = self.set_section_value(4)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  def set_data_faturamento data
    data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
    real_data = data.strftime("%Y%m%d")

    if real_data.length == 8
      self.set_section_value(1, real_data)
    else
      raise "#{self.get_id}: Data de Faturamento das Contas não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_data_vencimento_fatura data
    data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
    real_data = data.strftime("%Y%m%d")

    if real_data.length == 8
      self.set_section_value(2, real_data)
    else
      raise "#{self.get_id}: Data de Vencimento da Fatura não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_data_remessa_banco data
    data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
    real_data = data.strftime("%Y%m%d")

    if real_data.length == 8
      self.set_section_value(3, real_data)
    else
      raise "#{self.get_id}: Data de Remessa ao Banco não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_data_remessa_fisica data
    data = Date.strptime(data, "%Y%m%d") if data.is_a?(String)
    real_data = data.strftime("%Y%m%d")

    if real_data.length == 8
      self.set_section_value(4, real_data)
    else
      raise "#{self.get_id}: Data de Remessa Física não pode estar vazia
              Valor: #{data}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    self.set_section_value(5, reservado)
  end
end

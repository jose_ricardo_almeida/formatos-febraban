
#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Retorno do Débito Automático - Banco
class Febraban150F < FormatSection
  def initialize(master, versao = Febraban150::VERSAO)
    super(master, true, false)

    @versao = versao

    case @versao
      when '04'
        self.monta_versao_04
      when '05'
        self.monta_versao_05
      else
        raise "Seção F: Versão não suportada: #{@versao}"
    end
  end

  protected
  def monta_versao_04
    @section = Section.new({
        0 => Position.new(1, 1, false, 'F', true), # Código do Registro
        1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
        2 => Position.new(3, 4, false),            # Agência para Débito
        3 => Position.new(4, 14, false),           # Identificação Cliente Banco
        4 => Position.new(5, 8, true),             # Data Vencimento/Débito
        5 => Position.new(6, 15, true),            # Valor Original/Debitado
        6 => Position.new(7, 2, false),            # Código de Retorno
        7 => Position.new(8, 60, false),           # Uso da Empresa (Seção E)
        8 => Position.new(11, 20, false),          # Reservado pelo Sistema
        9 => Position.new(12, 1, true)             # Código de movimento (Seção E)
    })
  end

  def monta_versao_05
    @section = Section.new({
        0 => Position.new(1, 1, false, 'F', true), # Código do Registro
        1 => Position.new(2, 25, false),           # Identificação do Cliente Empresa
        2 => Position.new(3, 4, false),            # Agência para Débito
        3 => Position.new(4, 14, false),           # Identificação Cliente Banco
        4 => Position.new(5, 8, true),             # Data Vencimento/Débito
        5 => Position.new(6, 15, true),            # Valor Original/Debitado
        6 => Position.new(7, 2, false),            # Código de Retorno
        7 => Position.new(8, 60, false),           # Uso da Empresa (Seção E)
        8 => Position.new(9, 1, true),             # Tipo de Identificação (1-CPF, 2-CNPJ)
        9 => Position.new(10, 15, true),           # Identificação
        10 => Position.new(11, 4, false),          # Reservado pelo Sistema
        11 => Position.new(12, 1, true)            # Código de movimento (Seção E)
    })
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  public
  def process_section(file)
    case @versao
      when '04'
        self.processa_arquivo_04(file)
      when '05'
        self.processa_arquivo_05(file)
    end
  end

  protected
  def processa_arquivo_04(file)
    self.set_id_cliente_empresa     file[1..25]
    self.set_agencia_debito         file[26..29]
    self.set_id_cliente_banco       file[30..43]
    self.set_data_vencimento_debito file[44..51]
    self.set_valor                  file[52..66]
    self.set_codigo_retorno         file[67..68]
    self.set_obs_empresa            file[69..128]
    self.set_reservado              file[129..148]
    self.set_cod_movimento          file[149..149]
  end

  def processa_arquivo_05(file)
    self.set_id_cliente_empresa     file[1..25]
    self.set_agencia_debito         file[26..29]
    self.set_id_cliente_banco       file[30..43]
    self.set_data_vencimento_debito file[44..51]
    self.set_valor                  file[52..66]
    self.set_codigo_retorno         file[67..68]
    self.set_obs_empresa            file[69..128]
    self.set_tipo_id_cliente        file[129..129]
    self.set_id_cliente             file[130..144]
    self.set_reservado              file[145..148]
    self.set_cod_movimento          file[149..149]
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  public
  def is_valid?
    case @versao
      when '04'
        self.is_valid_04?
      when '05'
        self.is_valid_05?
    end
  end

  protected
  def is_valid_04?
    result = (self.get_id_cliente_empresa.length > 0 and
              self.get_id_cliente_banco.length > 0   and
              self.get_agencia_debito.length > 0     and
              !self.get_data_vencimento_debito.nil?  and
              self.get_valor > 0                     and
              (self.get_codigo_retorno >= 0 or
                  self.get_codigo_retorno <= 99)     and
              (self.get_cod_movimento == 0 or
                  self.get_cod_movimento == 1))
  end

  def is_valid_05?
    result = (self.get_id_cliente_empresa.length > 0 and
              self.get_id_cliente_banco.length > 0   and
              self.get_agencia_debito.length > 0     and
              !self.get_data_vencimento_debito.nil?  and
              self.get_valor > 0                     and
              (self.get_codigo_retorno >= 0 or
                  self.get_codigo_retorno <= 99)     and
              (self.get_tipo_id_cliente == 1 or
                  self.get_tipo_id_cliente == 2)     and
              self.get_id_cliente.to_s.length > 0    and
              (self.get_cod_movimento == 0 or
                  self.get_cod_movimento == 1))
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
  def get_id_cliente_empresa
    self.get_section_value(1)
  end

  def get_agencia_debito
    self.get_section_value(2)
  end

  def get_id_cliente_banco
    self.get_section_value(3)
  end

  def get_data_vencimento_debito
    data = self.get_section_value(4)
    Date.new(data[0..3].to_i, data[4..5].to_i, data[6..7].to_i)
  end

  def get_valor
    self.get_section_value(5).to_i
  end

  def get_codigo_retorno
    self.get_section_value(6).to_i
  end

  def get_obs_empresa
    self.get_section_value(7)
  end

  def get_tipo_id_cliente
    self.get_section_value(8).to_i
  end

  def get_id_cliente
    self.get_section_value(9)
  end

  def get_cod_movimento
    case @versao
      when '04'
        self.get_section_value(9).to_i
      when '05'
        self.get_section_value(11).to_i
    end
  end

  def success?
    self.get_codigo_retorno() == 0
  end

  def soft_bounce?
    codes = [1, 5, 18, 31]
    !self.success? && codes.include?(self.get_codigo_retorno())
  end

  def hard_bounce?
    !self.success? && !self.soft_bounce?
  end

  def get_mensagem_retorno
    case self.get_codigo_retorno
    when 0
      "Débito efetuado"
    when 1
      "Débito não efetuado - Insuficiência de fundos"
    when 2
      "Débito não efetuado - Conta não cadastrada"
    when 4
      "Débito não efetuado - Outras restrições"
    when 5
      "Débito não efetuado – valor do débito excede valor limite aprovado"
    when 10
      "Débito não efetuado - Agência em regime de encerramento"
    when 12
      "Débito não efetuado - Valor inválido"
    when 13
      "Débito não efetuado - Data de lançamento inválida"
    when 14
      "Débito não efetuado - Agência inválida"
    when 15
      "Débito não efetuado - conta inválida"
    when 18
      "Débito não efetuado - Data do débito anterior à do processamento"
    when 19
      "Débito não efetuado – Agência/Conta não pertence ao CPF/CNPJ informado"
    when 20
      "Débito não efetuado – conta conjunta não solidária"
    when 30
      "Débito não efetuado - Sem contrato de débito automático"
    when 31
      "Débito efetuado em data diferente da data informada – feriado na praça de débito"
    when 96
      "Manutenção do Cadastro"
    when 97
      "Cancelamento - Não encontrado"
    when 98
      "Cancelamento - Não efetuado, fora do tempo hábil"
    when 99
      "Cancelamento - cancelado conforme solicitação"
    end
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  public
  def set_id_cliente_empresa id_cliente_empresa
    id_cliente_empresa = id_cliente_empresa.to_s

    if id_cliente_empresa.length > 0
      self.set_section_value(1, id_cliente_empresa)
    else
      raise "#{self.get_id}: Identificação do Cliente da Empresa não pode estar vazio
              Valor: #{id_cliente_empresa}"
    end
  end

  def set_agencia_debito agencia_debito
    agencia_debito = agencia_debito.to_s

    if agencia_debito.length > 0
      self.set_section_value(2, agencia_debito)
    else
      raise "#{self.get_id}: Agência do Débito não pode estar vazia
              Valor: #{agencia_debito}"
    end
  end

  def set_id_cliente_banco id_cliente_banco
    id_cliente_banco = id_cliente_banco.to_s

    if id_cliente_banco.length > 0
      self.set_section_value(3, id_cliente_banco)
    else
      raise "#{self.get_id}: Identificação do Cliente do Banco não pode estar vazio
              Valor: #{id_cliente_banco}"
    end
  end

  def set_data_vencimento_debito data_vencimento
      data_vencimento = Date.strptime(data_vencimento, "%Y%m%d") if data_vencimento.is_a?(String)
      real_data = data_vencimento.strftime("%Y%m%d")

      if real_data.length == 8
      self.set_section_value(4, real_data)
    else
      raise "#{self.get_id}: Data de Vencimento não pode estar vazia
              Valor: #{data_vencimento}"
    end
  end

  def set_valor valor
    valor = valor.to_i

    if valor > 0
      self.set_section_value(5, valor)
    else
      raise "#{self.get_id}: Valor deve ser positivo e maior que 0
              Valor: #{valor}"
    end
  end

  def set_codigo_retorno codigo
    numero = codigo.to_i

    if numero >= 0 and numero <= 99
      self.set_section_value(6, codigo)
    else
      raise "#{self.get_id}: Valor deve ser positivo e maior que 0
              Valor: #{valor}"
    end
  end

  def set_obs_empresa obs_empresa
    obs_empresa = obs_empresa.to_s

    if obs_empresa.length > 0
      self.set_section_value(7, obs_empresa)
    else
      raise "#{self.get_id}: Observação da Empresa não pode estar vazia
              Valor: #{obs_empresa}"
    end
  end

  def set_tipo_id_cliente tipo_id_cliente
    tipo_id_cliente = tipo_id_cliente.to_i

    if tipo_id_cliente == 1 or tipo_id_cliente == 2
      self.set_section_value(8, tipo_id_cliente)
    else
      raise "#{self.get_id}: Tipo de Identificação do Cliente deve ser:
              1 - CNPJ; 2 - CPF
              Valor: #{tipo_id_cliente}"
    end
  end

  def set_id_cliente id_cliente
    id_cliente = id_cliente.to_s
    id_cliente.to_i

    if id_cliente.length >= 13 or id_cliente.length == 0
      self.set_section_value(9, id_cliente)
    else
      raise "#{self.get_id}: Identificação do Cliente deve ser CPF ou CNPJ:
              CNPJ: 999999999 = Número, 9999 = Filial, e 99 = DV
              CPF: 0000999999999 = Número, 99 = DV
              Valor: #{id_cliente}"
    end
  end

  def set_reservado reservado
    reservado = reservado.to_s
    case @versao
      when '04'
        self.set_section_value(8, reservado)
      when '05'
        self.set_section_value(10, reservado)
    end
  end

  def set_cod_movimento cod_movimento
    cod_movimento = cod_movimento.to_i

    if cod_movimento == 0 or cod_movimento == 1
      case @versao
        when '04'
          self.set_section_value(9, cod_movimento)
        when '05'
          self.set_section_value(11, cod_movimento)
      end
    else
      raise "#{self.get_id}: Código de Movimentação deve ser de Débito Nacional ou Cnacelamento:
              0 = Débito Normal
              1 = Cancelamento (exclusão) de lançamento enviado anteriormente para o Banco
              Valor: #{cod_movimento}"
    end
  end
end

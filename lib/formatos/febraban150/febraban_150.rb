
class Febraban150
  require 'date'

  VersaoDisponivel = [ '04', '05' ]
  VERSAO = '05'

  # Carrega todas as dependências de Layout
  Dir[File.join(File.dirname(__FILE__), '../..',  'base', '**/*.rb')].sort.each do |file|
    require file
  end

  # Layout:
  # http://www.febraban.org.br/7Rof7SWg6qmyvwJcFwF7I0aSDf9jyV/sitefebraban/Layout%20D%E9bito%20Vers%E3o%2005%20-%2029022008.pdf

  # Partes do Layout
  require_relative 'febraban_150_a' # Header - Empresa e Banco
  require_relative 'febraban_150_b' # Cadastro Débito Automático - Banco
  require_relative 'febraban_150_c' # Ocorrências - Empresa
  require_relative 'febraban_150_d' # Alteração da Identificação Cliente/Empresa - Empresa
  require_relative 'febraban_150_e' # Débito em Conta - Empresa
  require_relative 'febraban_150_f' # Retorno do Débito Automático - Banco
  require_relative 'febraban_150_h' # Ocorrência da Alteração Cliente/Empresa - Banco
  require_relative 'febraban_150_i' # Incentivo de Débito Automático - Empresa
  require_relative 'febraban_150_j' # Confirmação de Processamento - Empresa/Banco
  require_relative 'febraban_150_k' # Lei 10833, Cobrança não-cumulativa COFINS  - Empresa
  require_relative 'febraban_150_l' # Cronograma de Faturamento Contas/Tributos - Empresa
  require_relative 'febraban_150_t' # Total de Clientes debitados - Banco
  require_relative 'febraban_150_x' # Relação de Agências - Banco
  require_relative 'febraban_150_z' # Footer/Trailler - Empresa/Banco

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Construtor
  def initialize(file = '', versao = '')
    @sections = []
    @versao = versao

    self.valida_arquivo(file)
  end

  protected
  def valida_versao(file)
    @versao = (!@versao.nil? && !@versao.empty?) ? @versao : IO.readlines(file)[0][79..80]

    unless Febraban150::VersaoDisponivel.include?(@versao)
      raise "Versão #{@versao} não suportada.
            Utilizar versões #{Febraban150::VersaoDisponivel.inspect}"
    end
  end

  def valida_arquivo(file = '')
    if file.size > 0
      self.valida_versao(file)
      self.process_file file
    end
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Geral
  protected
    def add_section section
      if section.is_valid?
        self.sections << section
      else
        raise "Seção #{section.get_id} está inválida:
                #{section.errors.inspect}
                #{section.to_s}"
      end
    end

    def add_section_from_business section
      if section.is_empresa?
        self.add_section section
        self
      else
        raise "Seção #{section.get_id} não pode ser criada pela Empresa"
      end
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Criação das Partes
  public

    # Seção A
    def create_header(codigo_remessa, codigo_convenio, nome_empresa,
                      codigo_banco, nome_banco, numero_sequencial)

      if self.get_header.nil?
        section = self.get_new_section('A')

        section.set_codigo_remessa        codigo_remessa
        section.set_codigo_convenio       codigo_convenio
        section.set_nome_empresa          nome_empresa
        section.set_codigo_banco          codigo_banco
        section.set_nome_banco            nome_banco
        section.set_data_geracao          Time.new
        section.set_numero_sequencial     numero_sequencial
        section.set_versao_layout         @versao
        section.set_identificacao_servico Febraban150A::ID_SERVICO
        section.set_reservado             ''

        self.add_section_from_business section

      else
        raise 'Header já declarado!'
      end
    end

    # Seção C
    def create_occorencias(id_cliente_empresa, agencia_debito, id_cliente_banco,
                           ocorrencia_1, ocorrencia_2, cod_movimento)

      self.valida_existe_header

      section = self.get_new_section("C")

      section.set_id_cliente_empresa id_cliente_empresa
      section.set_agencia_debito     agencia_debito
      section.set_id_cliente_banco   id_cliente_banco
      section.set_ocorrencia_1       ocorrencia_1
      section.set_ocorrencia_2       ocorrencia_2
      section.set_reservado          ""
      section.set_cod_movimento      cod_movimento

      self.add_section_from_business section
    end

    # Seção D
    def create_alteracao_id(id_cliente_empresa_old, agencia_debito, id_cliente_banco,
                            id_cliente_empresa_new, ocorrencia, cod_movimento)

      self.valida_existe_header

      section = self.get_new_section("D")

      section.set_id_cliente_empresa_old id_cliente_empresa_old
      section.set_agencia_debito         agencia_debito
      section.set_id_cliente_banco       id_cliente_banco
      section.set_id_cliente_empresa_new id_cliente_empresa_new
      section.set_ocorrencia             ocorrencia
      section.set_reservado              ""
      section.set_cod_movimento          cod_movimento

      self.add_section_from_business section
    end

    # Seção E
    def create_debito_conta(id_cliente_empresa, agencia_debito, id_cliente_banco,
                            data_vencimento, valor_debito, codigo_moeda,
                            obs_empresa, tipo_id_cliente, id_cliente,
                            cod_movimento)

      self.valida_existe_header

      section = self.get_new_section("E")

      case @versao
        when '04'
          section.set_id_cliente_empresa id_cliente_empresa
          section.set_agencia_debito     agencia_debito
          section.set_id_cliente_banco   id_cliente_banco
          section.set_data_vencimento    data_vencimento
          section.set_valor_debito       valor_debito
          section.set_codigo_moeda       codigo_moeda
          section.set_obs_empresa        obs_empresa
          section.set_reservado          ''
          section.set_cod_movimento      cod_movimento
        when '05'
          section.set_id_cliente_empresa id_cliente_empresa
          section.set_agencia_debito     agencia_debito
          section.set_id_cliente_banco   id_cliente_banco
          section.set_data_vencimento    data_vencimento
          section.set_valor_debito       valor_debito
          section.set_codigo_moeda       codigo_moeda
          section.set_obs_empresa        obs_empresa
          section.set_tipo_id_cliente    tipo_id_cliente
          section.set_id_cliente         id_cliente
          section.set_reservado          ''
          section.set_cod_movimento      cod_movimento
      end

      self.add_section_from_business section
    end

    # Seção I
    def create_incentivo_debito(id_cliente_empresa, tipo_id_cliente, id_cliente,
                                nome, cidade, estado)

      self.valida_existe_header

      section = self.get_new_section("I")

      section.set_id_cliente_empresa id_cliente_empresa
      section.set_tipo_id_cliente    tipo_id_cliente
      section.set_id_cliente         id_cliente
      section.set_nome               nome
      section.set_cidade             cidade
      section.set_estado             estado
      section.set_reservado          ""

      self.add_section_from_business section
    end

    # Seção J
    def create_confirmacao_processo data_processamento
      self.valida_existe_header
      self.valida_existe_trailler

      raise "Confirmação do processo já executada" if self.get_section("J").length > 0

      header = self.get_header
      trailler = self.get_trailler

      section = self.get_new_section("J")

      section.set_numero_sequencial  header.get_numero_sequencial
      section.set_data_geracao       header.get_data_geracao
      section.set_total_processados  trailler.get_total_registros
      section.set_valor_total        trailler.get_valor_total
      section.set_data_processamento data_processamento
      section.set_reservado          ""

      self.add_section_from_business section
    end

    # Seção J
    def update_confirmacao_processo data_processamento
      self.valida_existe_header
      self.valida_existe_trailler
      self.valida_existe_confirmacao

      header = self.get_header
      trailler = self.get_trailler

      section = self.get_confirmacao

      section.set_numero_sequencial  header.get_numero_sequencial
      section.set_data_geracao       header.get_data_geracao
      section.set_total_processados  trailler.get_total_registros
      section.set_valor_total        trailler.get_valor_total
      section.set_data_processamento data_processamento
    end

    # Seção K
    def create_lei_10883(id_cliente_empresa, agencia_debito, id_cliente_banco,
                         tipo_tratamento, valor_debito, codigo_receita,
                         tipo_id_cliente, id_cliente)

      self.valida_existe_header
      valida_existe_section "E"

      section = self.get_new_section("K")

      section.set_id_cliente_empresa id_cliente_empresa
      section.set_agencia_debito     agencia_debito
      section.set_id_cliente_banco   id_cliente_banco
      section.set_tipo_tratamento    tipo_tratamento
      section.set_valor_debitado     valor_debito
      section.set_codigo_receita     codigo_receita
      section.set_tipo_id_cliente    tipo_id_cliente
      section.set_id_cliente         id_cliente
      section.set_reservado          ""

      self.add_section_from_business section
    end

    # Seção L
    def create_cronograma_faturamento(data_faturamento, data_vencimento,
                                      data_remessa_banco, data_remessa_fisica)

      self.valida_existe_header

      section = self.get_new_section("L")

      section.set_data_faturamento       data_faturamento
      section.set_data_vencimento_fatura data_vencimento
      section.set_data_remessa_banco     data_remessa_banco
      section.set_data_remessa_fisica    data_remessa_fisica
      section.set_reservado              ""

      self.add_section_from_business section
    end

    # Seção Z
    def create_trailler
      self.valida_existe_header

      if self.get_trailler.nil?
        if self.get_section("E").length > 0
          section = self.get_new_section("Z")
          section.set_reservado ""

          self.sections << section
          self.update_trailler
        else
          raise "Nenhum valor declarado (Seções E)!"
        end
      else
        raise "Trailler já declarado!"
      end
    end

    # Seção Z
    def update_trailler
      self.valida_existe_trailler

      if self.get_section("E").length > 0
        section = self.get_trailler
        section.set_total_registros self.sections.length
        section.set_valor_total     self.calculate_valor_total
      else
        raise "Nenhum valor declarado (Seções E)!"
      end
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Validações
  protected
    def valida_existe_header
      raise "Header (Seção A) ainda não declarado" if self.get_header.nil?
    end

    def valida_existe_trailler
      raise "Header (Seção Z) ainda não declarado" if self.get_trailler.nil?
    end

    def valida_existe_confirmacao
      raise "Confirmação (Seção J) ainda não declarada" if self.get_confirmacao.nil?
    end

    def valida_existe_section section
      raise "nenhum Registro da Seção #{section} foi declarado" if self.get_section(section).length <= 0
    end

    def valida_lei_10833
      secaoK = self.get_section("K")

      if secaoK.length > 0
        secaoE = self.get_section("E")

        raise "Deve haver um registro da Seção K para registro da Seção E.
               Existem #{secaoK.length} registros para K e #{secaoE.length} para a seção E" if secaoK.length == secaoE.length
      end
    end

    def is_empresa_valid?
      self.valida_existe_header
      self.valida_existe_section "E"
      self.valida_lei_10833
      self.valida_existe_trailler

      raise "Seção B inválida para Empresas" if self.get_section("B").length > 0
      raise "Seção F inválida para Empresas" if self.get_section("F").length > 0
      raise "Seção H inválida para Empresas" if self.get_section("H").length > 0

      true
    end

    def is_banco_valid?
      self.valida_existe_header
      self.valida_existe_section "F"
      self.valida_lei_10833
      self.valida_existe_trailler

      raise "Seção C inválida para Bancos" if self.get_section("C").length > 0
      raise "Seção D inválida para Bancos" if self.get_section("D").length > 0
      raise "Seção E inválida para Bancos" if self.get_section("E").length > 0

      true
    end

  public
    def is_valid?
      if self.layout_empresa?
        raise "Layout de Empresa inválido!" unless self.is_empresa_valid?

      elsif self.layout_banco?
        raise "Layout de Banco inválido!" unless self.is_banco_valid?

      else
        raise "Código de Remessa desconhecido: #{codigo_remessa}"
      end

      result = self.is_sections_valid?
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
    def compara_resultado outro_layout
      if self.layout_empresa?
        raise "Layout de comparação deve ser de Banco" unless outro_layout.layout_banco?

      elsif self.layout_banco?
        raise "Layout de comparação deve ser de Empresa" unless outro_layout.layout_empresa?

      else
        raise "Tipo de Layout não definido!"
      end
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
    def sections
      @sections.to_a
    end

    def has_section section
      self.get_section(section).length > 0
    end

    def get_section section
      self.sections.select {|c| c.is_section?(section) == true }
    end

    def is_sections_valid?
      self.sections.select {|b| b.is_valid? == false }.length == 0
    end

    def to_s
      self.sections.map {|a| a.to_s}.join("\r\n")
    end

    def get_header
      self.get_section("A").first()
    end

    def get_trailler
      self.get_section("Z").first()
    end

    def get_confirmacao
      self.get_section("J").first()
    end

    def get_valor_total
      self.get_trailler.get_valor_total
    end

    def versao_layout
      self.get_header.get_versao_layout
    end

    def identificacao_servico
      self.get_header.get_identificacao_servico
    end

    def layout_banco?
      self.get_header.get_codigo_remessa == 2
    end

    def layout_empresa?
      self.get_header.get_codigo_remessa == 1
    end

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# Utilitários
  public
    def save (path)
      raise "Layout atual inválido!" unless self.is_valid?
      raise "Caminho para salvamento inválido!" if path.length < 0

      if path.end_with?('/')
        header = self.get_header
        path += "#{header.get_data_geracao}_#{header.get_codigo_remessa}_#{header.get_codigo_banco}_#{self.versao_layout}.bat"
      end

      puts "Salvando arquivo: '#{path}'..."
      content = self.to_s

      File.open(path, 'w') do |file|
        file.truncate(0)
        file.write(content)
      end
      puts "Arquivo '#{path}' salvo!"

      self
    end

  protected
    def calculate_valor_total
      self.get_section("E").inject(0) do |sum, section|
        sum += section.get_valor_debito
      end
    end

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# Carregamento de Arquivo
  protected
    def process_file location
      file = File.new(location, 'r')

      while (line = file.gets)
        process_string line
      end

      file.close
    end

    def process_string file
        section_type = file[0,1]

        section = get_new_section(section_type)
        section.process_section(file)

        self.add_section section
    end

    def get_new_section section_type
      case section_type
        when "A"
          Febraban150A.new(self, @versao)
        when "B"
          Febraban150B.new(self, @versao)
        when "C"
          Febraban150C.new(self, @versao)
        when "D"
          Febraban150D.new(self, @versao)
        when "E"
          Febraban150E.new(self, @versao)
        when "F"
          Febraban150F.new(self, @versao)
        when "H"
          Febraban150H.new(self, @versao)
        when "I"
          Febraban150I.new(self, @versao)
        when "J"
          Febraban150J.new(self, @versao)
        when "K"
          Febraban150K.new(self, @versao)
        when "L"
          Febraban150L.new(self, @versao)
        when "T"
          Febraban150T.new(self, @versao)
        when "X"
          Febraban150X.new(self, @versao)
        when "Z"
          Febraban150Z.new(self, @versao)
      end
    end
end

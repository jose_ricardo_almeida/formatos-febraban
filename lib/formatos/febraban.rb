require 'formatos/febraban/version'
require 'i18n'

# Carrega todas as dependências
Dir[File.join(File.dirname(__FILE__), '../..',  'base', '**/*.rb')].sort.each do |file|
  require file
end

require 'formatos/febraban150/febraban_150'
require 'formatos/cielo/cielo'
require 'formatos/cnab400/cnab_400'

module Formatos
  module Febraban
    # Your code goes here...
    I18n.enforce_available_locales = false
  end
end

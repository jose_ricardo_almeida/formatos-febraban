# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'formatos/febraban/version'

Gem::Specification.new do |spec|
  spec.name          = "formatos-febraban"
  spec.version       = Formatos::Febraban::VERSION
  spec.authors       = ["José Ricardo Almeida"]
  spec.email         = ["jose@trackmob.com.br"]

  spec.summary       = "Formatos bancários EDI FEBRABAN"
  spec.description   = "Formatos para envio e recebimento de dados bancários da FEBRABAN"
  spec.homepage      = "https://bitbucket.org/jose_ricardo_almeida/formatos-febraban"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "byebug"
end



class FormatSection

  def initialize(master, responsabilidade_banco, responsabilidade_empresa,
                 permite_duplicata = true)

    @master = master
    @section = :nil
    @errors = []
    @responsabilidade_banco  = responsabilidade_banco
    @responsabilidade_empresa = responsabilidade_empresa
    @permite_duplicata = permite_duplicata
  end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Gerais
  public
    def section
      @section
    end

    def master
      @master
    end

    def errors
      @errors
    end

  protected
    def get_section_value position
      self.section.get_position_value(position)
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Getters
  public
    def is_optional?
      self.section.is_optional?
    end

    def is_section? section
      self.section.is_section? section
    end

    def is_banco?
      @responsabilidade_banco
    end

    def is_empresa?
      @responsabilidade_empresa
    end

    def is_permite_duplicata?
      @permite_duplicata
    end

    def get_section_value position
      self.section.get_position_value(position)
    end

    def get_id
      self.get_section_value(0).to_s
    end

    def to_s
      self.section.to_s
    end

#-------------------------------------------------------------------
#-------------------------------------------------------------------
# Setters
  protected
    def set_section_value(position, value)
      self.section.set_position_value(position, value)
    end
end

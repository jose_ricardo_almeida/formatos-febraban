
class Position
  require 'i18n'

  def initialize(position, length, numbers_only,
                 content = '', immutable = false, obligatory = false)

    @position     = position
    @length       = length
    @numbers_only = numbers_only
    @content      = adjust_content(content)
    @immutable    = immutable
    @obligatory   = obligatory
  end

  def content
    @content
  end

  def length
    @length
  end

  def is_obligatory?
    @obligatory
  end

  def is_empty?
    @content == ""
  end

  def is_numbers_only?
    @numbers_only
  end

  def is_immutable?
    @immutable
  end

  def is_valid?
    self.is_obligatory? ? !self.is_empty? : true
  end

  def to_s
    self.content.to_s
  end

  def to_clean_s
    self.to_s.strip()
  end

  def set_content(content)
    if not @immutable
      if self.is_numbers_only? and !self.is_number(content)
        raise "Posição #{@position} aceita apenas valores numéricos
                Valor: #{content}"
      end

      @content = self.adjust_content(content)

    else
      raise 'Valor imutável'
    end
  end

  def is_number(value)
    true if Float(value) rescue false
  end

  protected
  def adjust_content(string)
    string = I18n.transliterate(string.to_s)

    if string.length > self.length
      string = self.reduce_string string

    elsif string.length < self.length
      if self.is_numbers_only?
        string = string.rjust(self.length, '0')

      else
        string = string.ljust(self.length, ' ')
      end
    end

    string.upcase
  end

  def reduce_string(string)
    string[0 .. self.length - 1]
  end
end

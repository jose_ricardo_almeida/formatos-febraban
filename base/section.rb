
class Section
  require_relative 'position'

  def initialize(position_hash, optional = false)
    @positions = position_hash
    @optional = optional
  end

  def is_optional?
    @optional
  end

  def to_s
    self.positions.map{ |k, v| v.to_s }.join()
  end

  def positions
    @positions.to_a
  end

  def get_position position
    self.positions[position.to_i][1]
  end

  def get_position_value position
    self.get_position(position).to_clean_s
  end

  def is_section? section
    self.positions.select {|b| b[1].content == section }.length > 0
  end

  def is_valid?
    self.positions.select {|b| b[1].is_valid? == false }.length < 1
  end

  def set_position_value(position, content)
    @positions[position.to_i].set_content content
  end
end
